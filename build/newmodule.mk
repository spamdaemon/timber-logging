
.PHONY: new-module

module/% :
	@if [ -e $(PROJECT_DIR)/$(patsubst module/%,modules/%,$@) ]; then \
	  echo Module $(patsubst module/%,modules/%,$@) already exists; \
	  false; \
        else \
	  mkdir -p $(PROJECT_DIR)/$(patsubst module/%,modules/%,$@); \
	  mkdir -p $(PROJECT_DIR)/$(patsubst module/%,modules/%,$@)/build; \
	  cp $(PROJECT_DIR)/build/new_module/Makefile $(PROJECT_DIR)/$(patsubst module/%,modules/%,$@); \
#	  cp -r $(PROJECT_DIR)/build/new_module/build $(PROJECT_DIR)/$(patsubst module/%,modules/%,$@); \
	fi;
