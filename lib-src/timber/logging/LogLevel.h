#ifndef _TIMBER_LOGGING_LOGLEVEL_H
#define _TIMBER_LOGGING_LOGLEVEL_H

#ifndef _TIMBER_LOGGING_LEVEL_H
#include <timber/logging/Level.h>
#endif

#include <memory>

namespace timber {
   namespace logging {

      /** A log level wraps a Level object */
      class LogLevel
      {
            LogLevel() = delete;
            LogLevel(const LogLevel&) = delete;
            LogLevel&operator=(const LogLevel&) = delete;

            /**
             * Default constructor.
             * @param ll the log level
             */
         public:
            LogLevel(Level ll);

            /**
             * Create a new log level
             * @return a new log level
             */
         public:
            static ::std::shared_ptr< LogLevel> create(Level ll);

            /**
             * Create a new log level corresponding to OFF.
             * @return a log level that is OFF
             */
         public:
            static ::std::shared_ptr< const LogLevel> off();

            /**
             * Set the log level.
             * @param newLevel the new log level
             */
         public:
            void setLevel(Level newLevel);

            /**
             * Get the log level
             * @return the current log level
             */
         public:
            Level level() const;

            /**
             * Determine if entries at the specified log level will be committed to this log.
             * Only levels at or below the current log's log level will be considered.
             * This method must not return true if the level exceeds the current log level.
             * @param loglevel a log level
             * @return true if the entries will be logged at the specified level
             * @note a logLevel of Level::OFF is never loggable
             */
         public:
            bool isLoggable(Level logLevel) const;

            /** The log level */
         private:
            Level _level;

            /** The log level as a numeric id */
         private:
            ::std::int32_t _effectiveLevel;
      };

   }
}

#endif
