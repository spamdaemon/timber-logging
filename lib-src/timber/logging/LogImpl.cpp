#include <timber/logging/ConsoleLogHandler.h>
#include <timber/logging/LogFactory.h>
#include <timber/logging/LogImpl.h>
#include <timber/logging/LogRecord.h>
#include <iostream>
#include <iterator>
#include <new>
#include <string>
#include <mutex>

namespace timber {
   namespace logging {
      namespace {

         struct Logger : public LogImpl
         {
               Logger(::std::shared_ptr< LogImpl> xparent, ::std::string nm)
                     : LogImpl(::std::move(nm), xparent->level()->level()), _parent(::std::move(xparent))
               {
               }

               ~Logger()
               {
               }

               void logEntry(::std::shared_ptr< LogRecord> entry)
               {
                  LogImpl::logEntry(entry);
                  _parent->log(::std::move(entry));
               }

            private:
               ::std::shared_ptr< LogImpl> _parent;
         };

         /** The default function */
         struct Factory : public LogFactory
         {
               Factory()
                     : _handler(::std::make_shared< ConsoleLogHandler>(true))
               {
               }

               ~Factory()
               {
               }

               ::std::shared_ptr< LogImpl> newLog(::std::string log, ::std::shared_ptr< LogImpl> parent)
               {
                  ::std::shared_ptr< LogImpl> impl;
                  try {
                     if (parent) {
                        impl = ::std::make_shared< Logger>(::std::move(parent), ::std::move(log));
                        // we're not going to set the default handler here, because
                        // then the log messages would be emitted multiple times
                     }
                     else {
                        impl = ::std::make_shared< LogImpl>(::std::move(log), Level::INFO);
                        impl->addLogHandler(_handler);
                     }
                  }
                  catch (const ::std::exception& e) {
                     ::std::cerr << "Failed to create log entry" << ::std::endl;
                  }
                  return impl;
               }

            private:
               ::std::shared_ptr< LogHandler> _handler;
         };

         static ::std::shared_ptr< LogFactory> getset(::std::shared_ptr< LogFactory> f = nullptr)
         {
            static ::std::shared_ptr< LogFactory> theFactory(new Factory());
            if (f) {
               ::std::atomic_store(&theFactory, f);
            }
            return theFactory;
         }
      }

      void LogImpl::setFactory(::std::shared_ptr< LogFactory> m)

      {
         getset(::std::move(m));
      }

      ::std::shared_ptr< LogImpl> LogImpl::create(::std::string nm, Level level)

      {
         ::std::shared_ptr< LogImpl> res;
         auto f = getset();
         try {
            res = f->newLog(::std::move(nm), ::std::shared_ptr< LogImpl>());
            res->setLevel(level);
         }
         catch (const ::std::exception& e) {
            // need to ignore
            ::std::cerr << __FILE__ << ":" << __LINE__ << " failed to create  log" << ::std::endl;
         }
         return res;
      }

      LogImpl::LogImpl(::std::string nm, const Level& initLevel)

            : _name(::std::move(nm)), _level(LogLevel::create(initLevel))
      {
      }

      LogImpl::~LogImpl()
      {
      }

      ::std::shared_ptr< LogImpl> LogImpl::createChildLog(::std::string nm)

      {
         ::std::shared_ptr< LogImpl> res;
         auto f = getset();
         try {
            res = f->newLog(::std::move(nm), shared_from_this());
         }
         catch (const ::std::exception&) {
            ::std::cerr << __FILE__ << ":" << __LINE__ << " failed to create child log" << ::std::endl;
         }
         return res;
      }

      void LogImpl::setLevel(const Level& newLevel)
      {
         _level->setLevel(newLevel);
      }

      ::std::shared_ptr< const LogLevel> LogImpl::level() const
      {
         return _level;
      }

      void LogImpl::addLogHandler(::std::shared_ptr< LogHandler> newHandler)
      {
         try {
            ::std::lock_guard< ::std::mutex> guard(_mutex);
            if (!_handlers) {
               _handlers.reset(new Handlers());
            }

            for (const auto& handler : *_handlers) {
               if (handler == newHandler) {
                  // since the handler already exists, we don't need to do anything
                  return;
               }
            }
            _handlers->push_back(newHandler);
         }
         catch (const ::std::invalid_argument& e) {
            throw e;
         }
         catch (const ::std::exception& e) {
            ::std::cerr << __FILE__ << ":" << __LINE__ << ":LogImpl::addLogHandler: " << e.what() << ::std::endl;
         }
      }

      void LogImpl::removeLogHandler(::std::shared_ptr< LogHandler> handler)
      {
         if (handler) {
            try {
               ::std::lock_guard< ::std::mutex> guard(_mutex);
               if (_handlers) {
                  for (Handlers::iterator i = _handlers->begin(); i != _handlers->end(); ++i) {
                     if (*i == handler) {
                        _handlers->erase(i);
                        break;
                     }
                  }
               }
            }
            catch (const ::std::exception& e) {
               ::std::cerr << __FILE__ << ":" << __LINE__ << ":LogImpl::removeLogHandler: " << e.what() << ::std::endl;
            }
         }
      }

      void LogImpl::removeLogHandlers()
      {
         try {
            ::std::lock_guard< ::std::mutex> guard(_mutex);
            _handlers = nullptr;
         }
         catch (const ::std::exception& e) {
            ::std::cerr << __FILE__ << ":" << __LINE__ << ":LogImpl::removeLogHandlers: " << e.what() << ::std::endl;
         }
      }

      void LogImpl::log(::std::shared_ptr< LogRecord> e)
      {
         if (isLoggable(e->level())) {
            try {
               ::std::lock_guard< ::std::mutex> guard(_mutex);
               logEntry(::std::move(e));
            }
            catch (const ::std::exception& ex) {
               ::std::cerr << __FILE__ << ":" << __LINE__ << ":LogImpl::log: " << ex.what() << ::std::endl;
            }
         }
      }

      void LogImpl::logEntry(::std::shared_ptr< LogRecord> e)
      {
         if (_handlers) {
            for (auto& handler : *_handlers) {
               handler->processLogEntry(e);
            }
         }
      }

   }
}

