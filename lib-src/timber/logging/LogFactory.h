#ifndef _TIMBER_LOGGING_LOGFACTORY_H
#define _TIMBER_LOGGING_LOGFACTORY_H

#ifndef _TIMBER_LOGGING_LOGIMPL_H
#include <timber/logging/LogImpl.h>
#endif

namespace timber {
   namespace logging {

      /**
       * The LogFactory is used to create log instances.
       */
      class LogFactory
      {
            LogFactory(const LogFactory &) = delete;
            LogFactory & operator=(const LogFactory &) = delete;

            /** Default constructor  */
         protected:
            LogFactory();

            /** The destructor */
         protected:
            virtual ~LogFactory();

            /**
             * Create a new log. If the name is the empty string,
             * then an anonymous log will be created.
             * @param name the name of the new log
             * @param parent the parent log if there is any
             */
         public:
            virtual ::std::shared_ptr< LogImpl> newLog(::std::string name, ::std::shared_ptr< LogImpl> parent) = 0;

      };

   }
}

#endif
