#include <timber/logging/Log.h>
#include <timber/logging/LogRecord.h>
#include <timber/logging/Registry.h>
#include <sstream>
#include <iostream>
#include <atomic>
#include <cassert>

#define TIMBER_LOG_METHOD_LEVEL(METHOD,LEVEL) \
   void Log::METHOD(const MessageProducer& producer) { if (isLoggable (Level::LEVEL)) { logInternal(Level::LEVEL, producer); } } \
   void Log::METHOD(const LogProducer& producer) { if (isLoggable (Level::LEVEL)) { logInternal(Level::LEVEL, producer); } } \
   void Log::METHOD(const char* message) { if (isLoggable (Level::LEVEL)) { logInternal(Level::LEVEL, message); } } \
   void Log::METHOD(::std::string message) { if (isLoggable (Level::LEVEL)) { logInternal(Level::LEVEL, ::std::move(message)); } } \
   void Log::METHOD(::std::string message, const ::std::exception& e) { if (isLoggable (Level::LEVEL)) { logInternal(Level::LEVEL, ::std::move(message), e); } }

namespace timber {
   namespace logging {
      namespace {

         static Log findLog(const char* n)
         {
            ::std::shared_ptr< LogImpl> p;
            if (n) {
               p = Registry::findLog(n, true);
            }
            if (p) {
               return Log(p);
            }
            else {
               return Log::rootLog();
            }
         }

         static Log findLog(const ::std::string& n)
         {
            ::std::shared_ptr< LogImpl> p(Registry::findLog(n, true));
            if (p) {
               return Log(p);
            }
            else {
               return Log::rootLog();
            }
         }

      }

      const char Log::ROOT_LOG_NAME[] = { "*GLOBAL*" };

      Log Log::rootLog()
      {
         static ::std::shared_ptr< LogImpl> root(LogImpl::create(ROOT_LOG_NAME, Level::OFF));
         if (root == nullptr) {
            ::std::cerr << "Could not create the root log" << ::std::endl;
         }
         return Log(root);
      }

      Log::Log(::std::shared_ptr< LogImpl> a)
            : _impl(::std::move(a))
      {
         if (_impl) {
            _level = _impl->level();
         }
         else {
            _level = LogLevel::off();
         }
      }

      Log::Log()
            : Log(rootLog())
      {
         // consider using the LogContext here
      }

      Log::Log(const char* n)
            : Log(findLog(n)._impl)
      {
      }

      Log::Log(const ::std::string& n)
            : Log(findLog(n)._impl)
      {
      }
      Log Log::create(const ::std::string& n, Level level) const
      {
         auto res = create(n);
         res.setLevel(level);
         return res;
      }

      Log Log::create(const ::std::string& n) const
      {
         ::std::shared_ptr< LogImpl> res = _impl->createChildLog(n);
         if (!res) {
            ::std::cerr << "Failed to create child log '" << n << "' of log " << name() << ::std::endl;
         }
         return Log(res);
      }

      void Log::setLevel(const Level& newLevel)
      {
         if (_impl) {
            _impl->setLevel(newLevel);
            _level = _impl->level();
         }
      }

      ::std::string Log::name() const
      {
         ::std::string res;
         if (_impl) {
            res = _impl->name();
         }
         return res;
      }

      Level Log::level() const
      {
         return _level->level();
      }

      void Log::logInternal(const Level& logLevel, ::std::string msg, const ::std::exception& e)
      {
         assert(_impl);
         ::std::string location;
         ::std::shared_ptr< LogRecord> entry(LogRecord::create(name(), logLevel, location, ::std::string()));
         if (entry) {
            msg += " : ";
            msg += typeid(e).name();
            msg += " : ";
            msg += e.what();
            entry->setMessage(::std::move(msg));
            _impl->log(entry);
         }
      }

      void Log::logInternal(const Level& logLevel, ::std::string s)
      {
         assert(_impl);
         ::std::string location;
         ::std::shared_ptr< LogRecord> e(LogRecord::create(name(), logLevel, location, ::std::move(s)));
         if (e) {
            _impl->log(e);
         }
      }

      void Log::logInternal(const Level& logLevel, const char* msg)
      {
         assert(_impl);
         if (msg) {
            ::std::string location;
            ::std::shared_ptr< LogRecord> e(LogRecord::create(name(), logLevel, location, msg));
            if (e) {
               _impl->log(e);
            }
         }
      }

      void Log::logInternal(const Level& logLevel, const MessageProducer& producer)
      {
         assert(_impl);
         ::std::string location;
         ::std::shared_ptr< LogRecord> e(LogRecord::create(name(), logLevel, location, producer));
         if (e) {
            _impl->log(e);
         }
      }

      void Log::logInternal(const Level& logLevel, const LogProducer& producer)
      {
         assert(_impl);
         ::std::string location;
         ::std::shared_ptr< LogRecord> e(LogRecord::create(name(), logLevel, location, producer));
         if (e) {
            _impl->log(e);
         }
      }

      void Log::addLogHandler(::std::shared_ptr< LogHandler> handler)
      {
         if (_impl) {
            _impl->addLogHandler(::std::move(handler));
         }
      }

      void Log::removeLogHandlers()
      {
         if (_impl) {
            _impl->removeLogHandlers();
         }
      }

      void Log::removeLogHandler(::std::shared_ptr< LogHandler> handler)
      {
         if (_impl) {
            _impl->removeLogHandler(::std::move(handler));
         }
      }

      void Log::log(::std::shared_ptr< LogRecord> e)
      {
         if (e != nullptr && isLoggable(e->level())) {
            _impl->log(::std::move(e));
         }
      }

      void Log::log(::std::unique_ptr< LogRecord> e)
      {
         if (isLoggable(e->level())) {
            _impl->log(::std::move(e));
         }
      }

      void Log::log(const Level& logLevel, ::std::string message)
      {
         if (isLoggable(logLevel)) {
            logInternal(logLevel, ::std::move(message));
         }
      }

      void Log::log(const Level& logLevel, const char* message)
      {
         if (message && isLoggable(logLevel)) {
            logInternal(logLevel, message);
         }
      }
      void Log::log(const Level& logLevel, const MessageProducer& producer)
      {
         if (isLoggable(logLevel)) {
            logInternal(logLevel, producer);
         }
      }

      void Log::log(const Level& logLevel, const LogProducer& producer)
      {
         if (isLoggable(logLevel)) {
            logInternal(logLevel, producer);
         }
      }

      void Log::log(const Level& logLevel, ::std::string message, const ::std::exception& ex)
      {
         if (isLoggable(logLevel)) {
            logInternal(logLevel, ::std::move(message), ex);
         }
      }

      void Log::throwing(::std::string message, const ::std::exception& ex)
      {
         if (isLoggable(Level::THROWING)) {
            logInternal(Level::THROWING, ::std::move(message), ex);
         }
      }

      void Log::throwing(const ::std::exception& ex)
      {
         if (isLoggable(Level::THROWING)) {
            logInternal(Level::THROWING, "Throwing exception", ex);
         }
      }

      void Log::caught(::std::string message, const ::std::exception& ex)
      {
         if (isLoggable(Level::CAUGHT)) {
            logInternal(Level::CAUGHT, ::std::move(message), ex);
         }
      }

      void Log::caught(const ::std::exception& ex)
      {
         if (isLoggable(Level::CAUGHT)) {
            logInternal(Level::CAUGHT, "Caught exception", ex);
         }
      }

      void Log::caughtUnexpected()
      {
         if (isLoggable(Level::SEVERE)) {
            logInternal(Level::SEVERE, "Caught unexpected exception");
         }
      }

      TIMBER_LOG_METHOD_LEVEL(warn, WARN)
      TIMBER_LOG_METHOD_LEVEL(severe, SEVERE)
      TIMBER_LOG_METHOD_LEVEL(bug, BUG)
      TIMBER_LOG_METHOD_LEVEL(debugging, DEBUGGING)
      TIMBER_LOG_METHOD_LEVEL(tracing, TRACING)
      TIMBER_LOG_METHOD_LEVEL(info, INFO)
   }
}
