#include <timber/logging/FileLocation.h>
#include <iostream>

namespace timber {
   namespace logging {

      FileLocation::FileLocation()
            : _line(__LINE__), _file(__FILE__), _function(__func__)
      {
      }

      FileLocation::FileLocation(const ::std::string& f, size_t l, const ::std::string& fn)
            : _line(l), _file(f), _function(fn)
      {
      }

      FileLocation::FileLocation(const char* f, size_t l, const char* fn)
            : _line(l), _file(f), _function(fn ? fn : "")
      {
      }

      FileLocation::~FileLocation()
      {
      }
   }
}

::std::ostream& operator<<(::std::ostream& out, const ::timber::logging::FileLocation& loc)
{
   out << loc.file() << ':' << loc.line();
   if (!loc.function().empty()) {
      out << '[' << loc.function() << ']';
   }
   return out;
}
