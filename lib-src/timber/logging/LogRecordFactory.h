#ifndef _TIMBER_LOGGING_LOGRECORDFACTORY_H
#define _TIMBER_LOGGING_LOGRECORDFACTORY_H

#ifndef _TIMBER_LOGGING_LOGRECORD_H
#include <timber/logging/LogRecord.h>
#endif

#include <string>

namespace timber {
   namespace logging {

      /** A log level */
      class Level;

      /**
       * The LogEntry factory provides the means to customize creation of log entries. The standard
       * parameters for a log entry are the name of the log, the log level, and the optional message.
       */
      class LogRecordFactory
      {
            LogRecordFactory(const LogRecordFactory &) = delete;
            LogRecordFactory & operator=(const LogRecordFactory &) = delete;

            /** Default constructor  */
         protected:
            LogRecordFactory();

            /** The destructor */
         protected:
            virtual ~LogRecordFactory() = 0;

            /**
             * Create a new log entry.
             * @param log the log that generated the entry
             * @param lx the log level
             * @param msg the msg or 0 if not specified
             */
         public:
            virtual ::std::shared_ptr< LogRecord> newRecord(::std::string log, const Level& lx, ::std::string location,
                  ::std::string msg) = 0;

      };

   }
}

#endif
