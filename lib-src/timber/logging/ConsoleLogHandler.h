#ifndef _TIMBER_LOGGING_CONSOLELOGHANDLER_H
#define _TIMBER_LOGGING_CONSOLELOGHANDLER_H

#ifndef _TIMBER_LOGGING_STREAMLOGHANDLER_H
#include <timber/logging/StreamLogHandler.h>
#endif

namespace timber {
  namespace logging {
    
    /**
     * This log handler writes log entries either to ::std::cerr or to ::std::cout. Only 
     * one ConsoleLogHandler per output stream should be created, because the output streams
     * will produce garbled output in a multithreaded application.
     */
    class ConsoleLogHandler : public StreamLogHandler {
      /** @name Disable copying
       * @{
       */
    private:
      ConsoleLogHandler(const ConsoleLogHandler&) = delete;
      ConsoleLogHandler& operator=(const ConsoleLogHandler&) = delete;
      /*@}*/
      
      /**
       * Creates a ConsoleLogHandler.
       * @param errStream if true use ::std::cerr, otherwise ::std::cout
       */
    public:
      ConsoleLogHandler (bool errStream) ;
      
      /**
       * Creates a ConsoleLogHandler that logs to ::std::cerr
       */
    public:
      ConsoleLogHandler () ;
      
      /**
       * Destructor
       */
    public:
      ~ConsoleLogHandler() ;

      /** The output stream to use */
    private:
      bool _cerr;
    };
  }
}

#endif

