#include <timber/logging/Level.h>
#include <cstring>

using namespace ::std;

namespace timber {
   namespace logging {
      namespace {

         static const ::std::int8_t LEVEL_ALL_ID = 127;
         static const ::std::int8_t LEVEL_DEBUGGING_ID = 100;
         static const ::std::int8_t LEVEL_TRACING_ID = 90;
         static const ::std::int8_t LEVEL_INFO_ID = 80;
         static const ::std::int8_t LEVEL_EXCEPTION_ID = 70;
         static const ::std::int8_t LEVEL_CAUGHT_ID = 60;
         static const ::std::int8_t LEVEL_THROWING_ID = 50;
         static const ::std::int8_t LEVEL_WARN_ID = 40;
         static const ::std::int8_t LEVEL_SEVERE_ID = 30;
         static const ::std::int8_t LEVEL_BUG_ID = 20;
         static const ::std::int8_t LEVEL_OFF_ID = -128;

         static const ::std::int8_t LEVEL_MAX_ID = LEVEL_ALL_ID;
         static const ::std::int8_t LEVEL_MIN_ID = LEVEL_BUG_ID;
      }

      const Level Level::MAX(LEVEL_MAX_ID);
      const Level Level::MIN(LEVEL_MIN_ID);

      const Level Level::ALL(LEVEL_ALL_ID);
      const Level Level::TRACING(LEVEL_TRACING_ID);
      const Level Level::DEBUGGING(LEVEL_DEBUGGING_ID);
      const Level Level::INFO(LEVEL_INFO_ID);
      const Level Level::CAUGHT(LEVEL_CAUGHT_ID);
      const Level Level::THROWING(LEVEL_THROWING_ID);
      const Level Level::EXCEPTION(LEVEL_EXCEPTION_ID);
      const Level Level::WARN(LEVEL_WARN_ID);
      const Level Level::SEVERE(LEVEL_SEVERE_ID);
      const Level Level::BUG(LEVEL_BUG_ID);
      const Level Level::OFF(LEVEL_OFF_ID);

      const char* Level::getName(Level x)
      {
         switch (x.id()) {
            case LEVEL_ALL_ID:
               return "ALL";
            case LEVEL_DEBUGGING_ID:
               return "DEBUGGING";
            case LEVEL_TRACING_ID:
               return "TRACING";
            case LEVEL_INFO_ID:
               return "INFO";
            case LEVEL_THROWING_ID:
               return "THROWING";
            case LEVEL_CAUGHT_ID:
               return "CAUGHT";
            case LEVEL_EXCEPTION_ID:
               return "EXCEPTION";
            case LEVEL_WARN_ID:
               return "WARN";
            case LEVEL_SEVERE_ID:
               return "SEVERE";
            case LEVEL_BUG_ID:
               return "BUG";
            case LEVEL_OFF_ID:
               return "OFF";
            default:
               return nullptr;
         };
      }

      Level Level::getLevel(const ::std::string& name)
      {
         return getLevel(name.c_str());
      }

      Level Level::getLevel(const char* name)
      {
         if (name == nullptr) {
            throw ::std::invalid_argument("nullptr");
         }
         if (::strcmp(name, "OFF") == 0) {
            return OFF;
         }
         if (::strcmp(name, "ALL") == 0) {
            return ALL;
         }
         if (::strcmp(name, "DEBUGGING") == 0) {
            return DEBUGGING;
         }
         if (::strcmp(name, "TRACING") == 0) {
            return TRACING;
         }
         if (::strcmp(name, "INFO") == 0) {
            return INFO;
         }
         if (::strcmp(name, "THROWING") == 0) {
            return THROWING;
         }
         if (::strcmp(name, "CAUGHT") == 0) {
            return CAUGHT;
         }
         if (::strcmp(name, "EXCEPTION") == 0) {
            return EXCEPTION;
         }
         if (::strcmp(name, "WARN") == 0) {
            return WARN;
         }
         if (::strcmp(name, "SEVERE") == 0) {
            return SEVERE;
         }
         if (::strcmp(name, "BUG") == 0) {
            return BUG;
         }
         throw ::std::invalid_argument(::std::string("No such log level ") + name);
      }

   }
}
