#include <timber/logging/ConsoleLogHandler.h>
#include <iostream>

namespace timber {
   namespace logging {

      ConsoleLogHandler::ConsoleLogHandler(bool usecerr)
            : _cerr(usecerr)
      {
         if (usecerr) {
            setStream(&::std::cerr);
         }
         else {
            setStream(&::std::cout);
         }
      }

      ConsoleLogHandler::ConsoleLogHandler()
            : ConsoleLogHandler(true)
      {
      }

      ConsoleLogHandler::~ConsoleLogHandler()
      {
      }

   }
}

