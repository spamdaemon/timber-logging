#ifndef _TIMBER_LOGGING_REGISTRY_H
#define _TIMBER_LOGGING_REGISTRY_H

#ifndef _TIMBER_LOGGING_LEVEL_H
#include <timber/logging/Level.h>
#endif

#ifndef _TIMBER_LOGGING_LOGIMPL_H
#include <timber/logging/LogImpl.h>
#endif

#include <vector>

namespace timber {
   namespace logging {

      /**
       * This class is used to register logs by their name
       * such that they can be accessed globally and used
       * for logging. Once a log is registered, it cannot be
       * removed.
       */
      class Registry
      {
            Registry(const Registry&) = delete;
            Registry&operator=(const Registry&) = delete;

            /** Disable construction */
         private:
            ~Registry();

            /** The default constructor */
         private:
            Registry();

            /**
             * Set the default log level for newly created logs.
             * @param level
             */
         public:
            static void setDefaultLevel (Level level);

            /**
             * Register a log. If the log was already registered, then
             * nothing is done.
             * @param log a log
             * @return log or nullptr if it was not registered
             */
         public:
            static ::std::shared_ptr< LogImpl> registerLog(::std::shared_ptr< LogImpl> log);

            /**
             * Find a log by the specified name. If the log by that
             * name does not exist, then one is created using
             * Logger::create(name,Level::INFO);
             * @param nm the name of a log
             * @param createIfNotFound if true, create a log by that name
             * @return a log or nullptr if not found.
             */
         public:
            static ::std::shared_ptr< LogImpl> findLog(const ::std::string& nm, bool createIfNotFound);

            /**
             * Find a log by the specified name.
             * @param nm the name of a log
             * @return a log or nullptr if not found.
             */
         public:
            inline static ::std::shared_ptr< LogImpl> findLog(const ::std::string& nm)
            {
               return findLog(nm, false);
            }

            /**
             * Get the set of registered logs.
             * @return the set of logs
             */
         public:
            static ::std::vector< ::std::shared_ptr< LogImpl> > logs();
      };

   }
}

#endif
