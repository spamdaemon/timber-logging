#ifndef _TIMBER_LOGGING_MACROS_H
#define _TIMBER_LOGGING_MACROS_H

#ifndef _TIMBER_LOGGING_LOGENTRY_H
#include <timber/logging/LogEntry.h>
#endif

#ifndef _TIMBER_LOGGING_LOG_H
#include <timber/logging/Log.h>
#endif

#ifndef _TIMBER_LOGGING_LEVEL_H
#include <timber/logging/Level.h>
#endif

/**
 * Start a new log entry using the specified log name and log level. Note, if the level is not
 * loggable, then the code between START_LOG and END_LOG are not executed.
 * @param LOGNAME the name of the log (usually a constant, but can be a variable)
 * @param LEVELNAME a Level, but without the Level:: prefix
 * @note this type of log must be terminated with END_LOG
 */
#define START_LOG(LOGNAME,LEVELNAME) \
   do { \
      if (::timber::logging::Log(LOGNAME).isLoggable(::timber::logging::Level::LEVELNAME)) { \
         ::timber::logging::LogEntry(LOGNAME,::timber::logging::Level::LEVELNAME).location(__FILE__,__LINE__)

#define CAUGHT_LOG(LOGNAME,EX) START_LOG(LOGAME,CAUGHT).exception(EX)
#define THROWING_LOG(LOGNAME,EX) START_LOG(LOGAME,THROWING).exception(EX)
#define EXCEPTION_LOG(LOGNAME,EX) START_LOG(LOGAME,EXCEPTION).exception(EX)

#define TRACING_LOG(LOGNAME) START_LOG(LOGAME,TRACING)
#define DEBUG_LOG(LOGNAME) START_LOG(LOGAME,DEBUG)
#define INFO_LOG(LOGNAME) START_LOG(LOGAME,INFO)
#define WARN_LOG(LOGNAME) START_LOG(LOGAME,WARN)
#define SEVERE_LOG(LOGNAME) START_LOG(LOGAME,SEVERE)
#define BUG_LOG(LOGNAME) START_LOG(LOGAME,BUG)

/**
 * End a log entry.
 * @note Must be terminated with a semi-colon
 */
#define END_LOG \
      ::timber::logging::commit; \
      }\
   }while(false)

/**
 * Log a simple message using START_LOG and END_LOG macros
 * @param LOGNAME the name of the log (usually a constant, but can be a variable)
 * @param LEVELNAME a Level, but without the Level:: prefix
 * @param MESSAGE a message
 */
#define LOG_MESSAGE(LOGNAME,LEVELNAME,MESSAGE) START_LOG(LOGNAME,LEVELNAME) << (MESSAGE) << END_LOG

#define CAUGHT_MESSAGE(LOGNAME,EX,MESSAGE) CAUGHT_LOG(LOGNAME,EX) << (MESSAGE) << END_LOG
#define THROWING_MESSAGE(LOGNAME,EX,MESSAGE) THROWING_LOG(LOGNAME,EX) << (MESSAGE) << END_LOG
#define EXCEPTION_MESSAGE(LOGNAME,EX,MESSAGE) EXCEPTION_LOG(LOGNAME,EX) << (MESSAGE) << END_LOG

#define TRACING_MESSAGE(LOGNAME,MESSAGE) TRACING_LOG(LOGNAME) << (MESSAGE) << END_LOG
#define DEBUG_MESSAGE(LOGNAME,MESSAGE) DEBUG_LOG(LOGNAME) << (MESSAGE) << END_LOG
#define INFO_MESSAGE(LOGNAME,MESSAGE) INFO_LOG(LOGNAME) << (MESSAGE) << END_LOG
#define WARN_MESSAGE(LOGNAME,MESSAGE) WARN_LOG(LOGNAME) << (MESSAGE) << END_LOG
#define SEVERE_MESSAGE(LOGNAME,MESSAGE) SEVERE_LOG(LOGNAME) << (MESSAGE) << END_LOG
#define BUG_MESSAGE(LOGNAME,MESSAGE) BUG_LOG(LOGNAME) << (MESSAGE) << END_LOG

#endif
