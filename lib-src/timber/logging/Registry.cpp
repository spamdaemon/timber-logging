#include <timber/logging/Registry.h>
#include <mutex>

#include <map>
#include <iostream>

namespace timber {
   namespace logging {
      namespace {

         static ::std::mutex mutex;

         typedef ::std::map< ::std::string, ::std::shared_ptr< LogImpl> > Loggers;

         static Loggers* loggers()
         {
            static ::std::map< ::std::string, ::std::shared_ptr< LogImpl> > logs;
            return &logs;
         }

         // should be fine for now
         static Level DEFAULT_LEVEL=Level::INFO;
      }

      Registry::~Registry()
      {
      }

      Registry::Registry()
      {
      }

      void Registry::setDefaultLevel(Level level)
      {
         DEFAULT_LEVEL=level;
      }

      ::std::shared_ptr< LogImpl> Registry::registerLog(::std::shared_ptr< LogImpl> log)
      {
         ::std::shared_ptr< LogImpl> res;
         try {
            ::std::lock_guard< ::std::mutex> guard(mutex);
            Loggers* logs = loggers();
            if (logs) {
               Loggers::iterator i = logs->find(log->name());
               if (i != logs->end()) {
                  if (i->second == log) {
                     // the log has already been registered
                     res = ::std::move(log);
                  }
                  else {
                     ::std::cerr << "Log with name " << log->name()
                           << " has already been registered with a different log" << ::std::endl;
                  }
               }
               else {
                  // ok, insert the log now
                  logs->insert(Loggers::value_type(log->name(), log));
                  res = ::std::move(log);
               }
            }
         }
         catch (const ::std::exception& e) {
            ::std::cerr << "Error when registering the log " << log->name() << "; log was not registered!"
                  << ::std::endl;
         }
         return res;
      }

      // FIXME: we need to have a find function that takes a string
      // otherwise, each we we do Log("foo") it makes a copy of the string!

      ::std::shared_ptr< LogImpl> Registry::findLog(const ::std::string& nm, bool createIfNotFound)
      {
         ::std::shared_ptr< LogImpl> res;
         try {
            ::std::lock_guard< ::std::mutex> guard(mutex);
            Loggers* logs = loggers();
            if (logs != 0) {
               Loggers::iterator i = logs->find(nm);
               if (i != logs->end()) {
                  res = i->second;
               }
               else if (createIfNotFound) {
                  // we use INFO as a default log for the registery
                  res = LogImpl::create(nm,DEFAULT_LEVEL);
                  if (res) {
                     logs->insert(Loggers::value_type(res->name(), res));
                  }
               }
            }
         }
         catch (const ::std::exception& e) {
            ::std::cerr << "Error when finding the log " << nm << ::std::endl;
         }
         return res;
      }

      ::std::vector< ::std::shared_ptr< LogImpl> > Registry::logs()
      {
         ::std::vector< ::std::shared_ptr< LogImpl> > res;
         try {
            ::std::lock_guard< ::std::mutex> guard(mutex);
            const Loggers* logs = loggers();
            if (logs != 0) {
               res.reserve(logs->size());
               for (auto i : *logs) {
                  res.push_back(i.second);
               }
            }
         }
         catch (const ::std::exception& e) {
            ::std::cerr << "Could not copy logs" << ::std::endl;
            res.clear();
         }
         return res;
      }
   }
}

