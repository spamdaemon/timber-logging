#ifndef _TIMBER_LOGGING_FILELOGHANDLER_H
#define _TIMBER_LOGGING_FILELOGHANDLER_H

#ifndef _TIMBER_LOGGING_STREAMLOGHANDLER_H
#include <timber/logging/StreamLogHandler.h>
#endif

#include <string>
#include <memory>

namespace timber {
   namespace logging {

      /**
       * This log handler implements the LogHandler interface for writing log entries to a file.
       */
      class FileLogHandler : public StreamLogHandler
      {
            /** @name Disable copying
             * @{
             */
         private:
            FileLogHandler(const FileLogHandler&) = delete;
            FileLogHandler& operator=(const FileLogHandler&) = delete;
            /*@}*/

            /**
             * Create a new log file handler. If the file already exists,
             * then it is erased. Automatic flushing is disabled.
             * @param name the log file name
             */
         public:
            FileLogHandler(const ::std::string& name);

            /**
             * Create a new log file handler. Automatic flushing is disabled.
             * If the file exists and append is false, then the file is erased,
             * otherwise log entries will be appended.
             * @param name the log file name
             * @param append true to append to an existing log file.
             */
         public:
            FileLogHandler(const ::std::string& name, bool append);

            /**
             * Destructor
             */
         public:
            ~FileLogHandler();

            /** The output stream */
         private:
            ::std::unique_ptr< ::std::ostream> _stream;
      };
   }
}

#endif
