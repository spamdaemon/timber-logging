#ifndef _TIMBER_LOGGING_LOGHANDLER_H
#define _TIMBER_LOGGING_LOGHANDLER_H

#include <iosfwd>
#include <memory>

namespace timber {
   namespace logging {

      /** A log entry */
      class LogRecord;

      /** A formatter */
      class Formatter;

      /**
       * The LogHandler is used by LogImpl instances to process individual log records.
       */
      class LogHandler
      {
            /** @name Disable copying
             * @{
             */
         private:
            LogHandler(const LogHandler&) = delete;
            LogHandler& operator=(const LogHandler&) = delete;

            /*@}*/

            /** Default constructor */
         protected:
            LogHandler();

            /** Destructor */
         protected:
            virtual ~LogHandler();

            /**
             * Set the formatter for this handler. If this handler does not support custom formatting
             * then this method may safely be ignored.
             * @param fmt a formatter or 0 to use a default formatter
             * @return true if the formatter was set, false if this method is ignored (default)
             */
         public:
            virtual bool setFormatter(const ::std::shared_ptr< Formatter>& fmt);

            /**
             * Flush any buffered log entries.
             * Does nothing by default.
             * @note Subclasses that perform any kind of buffering of log records objects are
             * required to flush them to guarantee that they are recorded before this function completes.
             */
         public:
            virtual void flush();

            /**
             * Process a new log entry.
             * @param entry a log entry
             */
         public:
            virtual void processLogEntry(const ::std::shared_ptr< LogRecord>& entry) = 0;

      };

   }
}

#endif
