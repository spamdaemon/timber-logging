#include <timber/logging/LogLevel.h>

namespace timber {
   namespace logging {

      LogLevel::LogLevel(Level ll)
            : _level(ll), _effectiveLevel(ll.id())
      {
         if (ll == Level::OFF) {
            --_effectiveLevel;
         }
      }

      ::std::shared_ptr< LogLevel> LogLevel::create(Level ll)
      {
         return ::std::make_shared< LogLevel>(ll);
      }

      ::std::shared_ptr< const LogLevel> LogLevel::off()
      {
         return create(Level::OFF);
      }

      void LogLevel::setLevel(Level newLevel)
      {
         _level = newLevel;
         _effectiveLevel = newLevel.id();
         if (newLevel == Level::OFF) {
            --_effectiveLevel;
         }
      }

      Level LogLevel::level() const
      {
         return _level;
      }

      bool LogLevel::isLoggable(Level logLevel) const
      {
         // the way we construct the level, we need to be strictly less
         return logLevel.id() <= _effectiveLevel;
      }
   }
}
