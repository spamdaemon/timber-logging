#ifndef _TIMBER_LOGGING_FILELOCATION_H
#define _TIMBER_LOGGING_FILELOCATION_H

#include <string>
#include <iosfwd>

#ifndef logLocation
#define logLocation ::timber::logging::FileLocation(__FILE__,__LINE__,__func__)
#endif

namespace timber {
   namespace logging {

      /**
       * This class stores a position in a file and a function. This
       * class is meant to be accessed via the location() macro.
       */
      class FileLocation
      {
            /** Default constructor */
         public:
            FileLocation();

            /**
             * Create a new file location.
             * @param f the file
             * @param line the line (counting at 1)
             * @param func the function (can be empty)
             */
         public:
            FileLocation(const ::std::string& f, size_t line, const ::std::string& func);

            /**
             * Create a new file location.
             * @param f the file
             * @param line the line (counting at 1)
             * @param func the function (can be empty)
             */
         public:
            FileLocation(const char* f, size_t line, const char* func);

            /** Destructor */
         public:
            ~FileLocation();

            /**
             * Get the file .
             * @return the file
             */
         public:
            inline const ::std::string& file() const
            {
               return _file;
            }

            /**
             * Get the line in the file
             * @return the line in the file
             */
         public:
            inline size_t line() const
            {
               return _line;
            }

            /**
             * Get the function .
             * @return the function
             */
         public:
            inline const ::std::string& function() const
            {
               return _function;
            }

            /**
             * This operator serves only 1 purposes, which is to allow the location
             * macro to be used like a function.
             */
         public:
            inline FileLocation& operator()()
            {
               return *this;
            }

            /** The line */
         private:
            size_t _line;

            /** The file  */
         private:
            ::std::string _file;

            /** The function  */
         private:
            ::std::string _function;
      };
   }
}

/**
 * Emit a file location to the specified stream
 * @param out the output stream
 * @param loc a file location
 * @return out
 */
::std::ostream& operator<<(::std::ostream& out, const ::timber::logging::FileLocation& loc);

#endif
