#include <timber/logging/LogRecord.h>

#include <cstdio>
#include <iostream>
#include <sstream>
#include <memory>
#include <atomic>

#include <timber/logging/LogRecordFactory.h>

namespace timber {
   namespace logging {
      namespace {

         struct Factory : public LogRecordFactory
         {
               ~Factory()
               {
               }

               ::std::shared_ptr< LogRecord> newRecord(::std::string log, const Level& lx, ::std::string location,
                     ::std::string msg)
               {
                  ::std::shared_ptr< LogRecord> res;
                  try {
                     return ::std::make_shared< LogRecord>(::std::move(log), lx, ::std::move(location),
                           ::std::move(msg));
                  }
                  catch (const ::std::exception& e) {
                     ::std::cerr << "Failed to create a log record" << ::std::endl;
                  }
                  return res;
               }
         };

         static ::std::shared_ptr< LogRecordFactory> getset(::std::shared_ptr< LogRecordFactory> f = nullptr)
         {
            ::std::shared_ptr< LogRecordFactory> theFactory(new Factory());
            if (f) {
               ::std::atomic_store(&theFactory, f);
            }
            return theFactory;
         }

         static inline ::std::shared_ptr< LogRecordFactory> getFactory()
         {
            return getset();
         }

         static inline LogRecord::Timestamp now()
         {
            return ::std::chrono::system_clock::now();
         }

      }
      void LogRecord::setFactory(::std::shared_ptr< LogRecordFactory> m)
      {
         getset(::std::move(m));
      }

      ::std::shared_ptr< LogRecord> LogRecord::create(::std::string log, const Level& lx, ::std::string location,
            const MessageProducer& producer)
      {
         ::std::shared_ptr< LogRecord> res;
         auto factory = getFactory();
         if (factory) {
            try {

               ::std::ostringstream oss;
               producer(oss);

               res = factory->newRecord(::std::move(log), lx, location, oss.str());
            }
            catch (...) {
            }
         }
         return res;
      }

      ::std::shared_ptr< LogRecord> LogRecord::create(::std::string log, const Level& lx, ::std::string location,
            const LogProducer& producer)
      {
         ::std::shared_ptr< LogRecord> res;
         auto factory = getFactory();
         if (factory) {
            try {
               res = factory->newRecord(::std::move(log), lx, location, producer());
            }
            catch (...) {
            }
         }
         return res;
      }

      ::std::shared_ptr< LogRecord> LogRecord::create(::std::string log, const Level& lx, ::std::string location,
            ::std::string msg)
      {
         ::std::shared_ptr< LogRecord> res;
         auto factory = getFactory();
         if (factory) {
            try {
               res = factory->newRecord(::std::move(log), lx, location, ::std::move(msg));
            }
            catch (...) {
            }
         }
         return res;
      }

      ::std::shared_ptr< LogRecord> LogRecord::create(::std::string log, const Level& lx, ::std::string location,
            const char* msg, ::std::int32_t n)
      {
         ::std::shared_ptr< LogRecord> res;
         if (!msg) {
            return res;
         }

         auto factory = getFactory();
         if (factory) {
            try {
               if (n < 0) {
                  res = factory->newRecord(::std::move(log), lx, location, ::std::string(msg));
               }
               else {
                  res = factory->newRecord(::std::move(log), lx, location, ::std::string(msg, 0, n));
               }
            }
            catch (const ::std::exception& e) {
               ::std::cerr << "Caught exception during log-record creation " << e.what() << ::std::endl;
            }
         }
         return res;
      }

      LogRecord::LogRecord(::std::string log, Level lx, ::std::string loc, ::std::string msg)
            : _log(::std::move(log)), _location(::std::move(loc)), _level(lx), _time(now()), _message(::std::move(msg))
      {
      }

      LogRecord::~LogRecord()
      {
      }

      void LogRecord::resetTime()
      {
         _time = now();
      }

      void LogRecord::setMessage(::std::string msg)
      {
         _message = ::std::move(msg);
      }

      void LogRecord::setMessage(const char* msg, ::std::int32_t n)
      {
         if (!msg || n == 0) {
            return;
         }

         if (n < 0) {
            _message += msg;
         }
         else {
            _message.append(msg, 0, n);
         }
      }

   }
}

