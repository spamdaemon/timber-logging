#include <timber/logging/LogContext.h>
#include <timber/logging/Log.h>
#include <timber/logging/LogImpl.h>

#include <stdexcept>

namespace timber {
   namespace logging {

      ::std::shared_ptr< LogImpl> LogContext::context(::std::shared_ptr< LogImpl> ctx)
      {
         thread_local static ::std::shared_ptr< LogImpl> CONTEXT(Log::rootLog()._impl);
         if (ctx) {
            ::std::swap(ctx, CONTEXT);
         }
         else {
            ctx = CONTEXT;
         }
         return ::std::move(ctx);
      }
      ::std::shared_ptr< LogImpl> LogContext::context(Log ctx)
      {
         return context(ctx._impl);
      }

      LogContext::LogContext()

            : _restore(context(Log::rootLog()))
      {
      }

      LogContext::LogContext(const char* l)

            : _restore(context(Log(l)))
      {
      }

      LogContext::LogContext(::std::string l)

            : _restore(context(Log(::std::move(l))))
      {
      }

      LogContext::LogContext(Log l)

            : _restore(context(l))
      {
      }

      LogContext::~LogContext()

      {
         context(_restore);
      }

      Log LogContext::log()

      {
         return Log(context(::std::shared_ptr< LogImpl>()));
      }

      void LogContext::log(const Level& logLevel, ::std::string message)

      {
         auto lg(context(::std::shared_ptr< LogImpl>()));
         if (lg) {
            Log(::std::move(lg)).log(logLevel, message);
         }
      }

      void LogContext::log(const Level& logLevel, const char* message)

      {
         auto lg(context(::std::shared_ptr< LogImpl>()));
         if (lg) {
            Log(::std::move(lg)).log(logLevel, message);
         }
      }

      void LogContext::log(const Level& logLevel, ::std::string message, const ::std::exception& e)

      {
         auto lg(context(::std::shared_ptr< LogImpl>()));
         if (lg) {
            Log(::std::move(lg)).log(logLevel, message, e);
         }
      }

      void LogContext::throwing(::std::string message, const ::std::exception& e)

      {
         auto lg(context(::std::shared_ptr< LogImpl>()));
         if (lg) {
            Log(::std::move(lg)).throwing(::std::move(message), e);
         }
      }

      void LogContext::throwing(const ::std::exception& e)

      {
         auto lg(context(::std::shared_ptr< LogImpl>()));
         if (lg) {
            Log(::std::move(lg)).throwing(e);
         }
      }

      void LogContext::caught(::std::string message, const ::std::exception& e)

      {
         auto lg(context(::std::shared_ptr< LogImpl>()));
         if (lg) {
            Log(::std::move(lg)).caught(::std::move(message), e);
         }
      }
      void LogContext::caught(const ::std::exception& e)

      {
         auto lg(context(::std::shared_ptr< LogImpl>()));
         if (lg) {
            Log(::std::move(lg)).caught(e);
         }
      }
      void LogContext::caughtUnexpected()

      {
         auto lg(context(::std::shared_ptr< LogImpl>()));
         if (lg) {
            Log(::std::move(lg)).caughtUnexpected();
         }

      }

      void LogContext::warn(const char* message)

      {
         auto lg(context(::std::shared_ptr< LogImpl>()));
         if (lg) {
            Log(::std::move(lg)).warn(::std::move(message));
         }
      }

      void LogContext::warn(::std::string message)

      {
         auto lg(context(::std::shared_ptr< LogImpl>()));
         if (lg) {
            Log(::std::move(lg)).warn(::std::move(message));
         }
      }

      void LogContext::warn(::std::string message, const ::std::exception& e)

      {
         auto lg(context(::std::shared_ptr< LogImpl>()));
         if (lg) {
            Log(::std::move(lg)).warn(::std::move(message), e);
         }
      }

      void LogContext::severe(const char* message)

      {
         auto lg(context(::std::shared_ptr< LogImpl>()));
         if (lg) {
            Log(::std::move(lg)).severe(::std::move(message));
         }
      }

      void LogContext::severe(::std::string message)

      {
         auto lg(context(::std::shared_ptr< LogImpl>()));
         if (lg) {
            Log(::std::move(lg)).severe(::std::move(message));
         }
      }

      void LogContext::severe(::std::string message, const ::std::exception& e)

      {
         auto lg(context(::std::shared_ptr< LogImpl>()));
         if (lg) {
            Log(::std::move(lg)).severe(::std::move(message), e);
         }
      }

      void LogContext::debugging(const char* message)

      {
         auto lg(context(::std::shared_ptr< LogImpl>()));
         if (lg) {
            Log(::std::move(lg)).debugging(::std::move(message));
         }
      }

      void LogContext::debugging(::std::string message)

      {
         auto lg(context(::std::shared_ptr< LogImpl>()));
         if (lg) {
            Log(::std::move(lg)).debugging(::std::move(message));
         }
      }

      void LogContext::debugging(::std::string message, const ::std::exception& e)

      {
         auto lg(context(::std::shared_ptr< LogImpl>()));
         if (lg) {
            Log(::std::move(lg)).debugging(::std::move(message), e);
         }
      }

      void LogContext::tracing(const char* message)

      {
         auto lg(context(::std::shared_ptr< LogImpl>()));
         if (lg) {
            Log(::std::move(lg)).tracing(::std::move(message));
         }
      }

      void LogContext::tracing(::std::string message)

      {
         auto lg(context(::std::shared_ptr< LogImpl>()));
         if (lg) {
            log(Level::TRACING, message);
         }
      }

      void LogContext::tracing(::std::string message, const ::std::exception& e)

      {
         auto lg(context(::std::shared_ptr< LogImpl>()));
         if (lg) {
            Log(::std::move(lg)).tracing(::std::move(message), e);
         }
      }

      void LogContext::info(const char* message)

      {
         auto lg(context(::std::shared_ptr< LogImpl>()));
         if (lg) {
            Log(::std::move(lg)).info(::std::move(message));
         }
      }

      void LogContext::info(::std::string message)

      {
         auto lg(context(::std::shared_ptr< LogImpl>()));
         if (lg) {
            Log(::std::move(lg)).info(::std::move(message));
         }
      }

      void LogContext::info(::std::string message, const ::std::exception& e)

      {
         auto lg(context(::std::shared_ptr< LogImpl>()));
         if (lg) {
            Log(::std::move(lg)).info(::std::move(message), e);
         }
      }

   }
}
