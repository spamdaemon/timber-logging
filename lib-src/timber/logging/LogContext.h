#ifndef _TIMBER_LOGGING_LOGCONTEXT_H
#define _TIMBER_LOGGING_LOGCONTEXT_H

#include <string>
#include <memory>

namespace timber {
   namespace logging {
      class LogImpl;
      class Log;
      class Level;

      /**
       * The LogContext enables use of logging capabilities without having to know the name
       * of a specific log. The LogContext is bound by the current scrope is thread-specific.
       * The following example illustrates how the use of LogContext can be
       * used to make logging easier.
       * @code
       *   // when starting this subsystem, use A.log
       *   void startSubsytemA() {
       *     // establish a log context until this is function exits
       *     LogContext context("A.log");
       *     doTask();
       *   }
       *
       *   // when starting this subsystem, use B.log
       *   void startSubsytemB() {
       *     // establish a log context until this is function exits
       *     LogContext context("B.log");
       *     doTask();
       *   }
       *
       *   void doTask() {
       *     LogContext::info("start function: doTask");
       *     ...
       *     LogContext::info("end function: doTask");
       *   }
       * @endcode
       * There is always an implicit root context which is used if no other context is
       * currently active.
       * @note There is some extra overhead associated with establishing and accessing a log context.
       * Some caching of the lookup maybe required if performance is a consideration.
       */
      class LogContext
      {
         private:
            LogContext(const LogContext&) = delete;
            LogContext& operator=(const LogContext&) = delete;

            /**
             * The default constructor activates the current
             * default log
             */
         public:
            LogContext();

            /**
             * Create a new log context
             * @param log the name of a log
             * @throws an exception if the context could not be created
             */
         public:
            LogContext(const char* log);

            /**
             * Create a new log context
             * @param log the name of a log
             * @throws an exception if the context could not be created
             */
         public:
            LogContext(::std::string log);

            /**
             * Create a new log context
             * @param l the log to be used
             * @throws an exception if the context could not be created
             */
         public:
            LogContext(Log l);

            /**
             * Destroy this log context
             */
         public:
            ~LogContext();

            /**
             * Get the current log. This function may return 0 in case
             * the global destructors have been activated.
             * @return the log in the current context or 0 if it cannot be obtained
             */
         public:
            static Log log();

            /**
             * @name Various log function. See Log::log() for more details.
             * @note If the log() method returns 0 then nothing will be logged.
             * @{
             */

            /**
             * Log a message. This method is equivalent to @code lg->log(logLevel,message) @endcode.
             * @param logLevel the log level
             * @param message the log message
             */
         public:
            static void log(const Level& logLevel, ::std::string message);

            /**
             * Log a message. This method is equivalent to @code lg->log(logLevel,message) @endcode.
             * @param logLevel the log level
             * @param message the log message
             */
         public:
            static void log(const Level& logLevel, const char* message);

            /**
             * Log an exception. This method is equivalent to @code lg->log(logLevel,message) @endcode.
             * @param logLevel the log level
             * @param message a log message for more info about the exception
             * @param e an exception
             */
         public:
            static void log(const Level& logLevel, ::std::string message, const ::std::exception& e);

            /**
             * Log an exception that is being thrown. This method is equivalent to
             * @code log(Level::THROWING,message,e) @endcode.
             * @param message a log message for more info about the exception
             * @param e an exception
             */
         public:
            static void throwing(::std::string message, const ::std::exception& e);

            /**
             * Log an exception that is being thrown. This method is equivalent to
             * @param e an exception
             */
         public:
            static void throwing(const ::std::exception& e);

            /**
             * Log an exception that has been caught. This method is equivalent to
             * @code log(Level::CAUGHT,message,e) @endcode.
             * @param message a log message for more info about the exception
             * @param e an exception
             */
         public:
            static void caught(::std::string message, const ::std::exception& e);

            /**
             * Log an exception that has been caught. This method is equivalent to
             * @param e an exception
             */
         public:
            static void caught(const ::std::exception& e);

            /**
             * Log that an unexpected exception that has been caught.
             * @see Log::caughtUnexpected()
             */
         public:
            static void caughtUnexpected();

            /**
             * Log a warning. This method is equivalent to
             * @code log(Level::WARN,message) @endcode
             * @param message a message
             */
         public:
            static void warn(const char* message);

            /**
             * Log a warning. This method is equivalent to
             * @code log(Level::WARN,message) @endcode
             * @param message a message
             */
         public:
            static void warn(::std::string message);

            /**
             * Log an exception at the warning level. This method is equivalent to
             * @code log(Level::WARN,message,e) @endcode
             * @param message a log message for more info about the exception
             * @param e an exception
             */
         public:
            static void warn(::std::string message, const ::std::exception& e);

            /**
             * Log a severe error message. This method is equivalent to
             * @code log(Level::SEVERE,message) @endcode
             * @param message a message
             */
         public:
            static void severe(const char* message);

            /**
             * Log a severe error message. This method is equivalent to
             * @code log(Level::SEVERE,message) @endcode
             * @param message a message
             */
         public:
            static void severe(::std::string message);

            /**
             * Log a severe exception. This method is equivalent to
             * @code log(Level::SEVERE,message,e) @endcode
             * @param message a log message for more info about the exception
             * @param e an exception
             */
         public:
            static void severe(::std::string message, const ::std::exception& e);

            /**
             * Log a bug error message. This method is equivalent to
             * @code log(Level::BUG,message) @endcode
             * @param message a message
             */
         public:
            static void bug(const char* message);

            /**
             * Log a bug error message. This method is equivalent to
             * @code log(Level::BUG,message) @endcode
             * @param message a message
             */
         public:
            static void bug(::std::string message);

            /**
             * Log a bug exception. This method is equivalent to
             * @code log(Level::BUG,message,e) @endcode
             * @param message a log message for more info about the exception
             * @param e an exception
             */
         public:
            static void bug(::std::string message, const ::std::exception& e);

            /**
             * Log a debug message. This method is equivalent to
             * @code log(Level::DEBUGGING,message) @endcode
             * @param message a message
             */
         public:
            static void debugging(const char* message);

            /**
             * Log a debug message. This method is equivalent to
             * @code log(Level::DEBUGGING,message) @endcode
             * @param message a message
             */
         public:
            static void debugging(::std::string message);

            /**
             * Log an exception at the DEBUGGING level. This method is equivalent to
             * @code log(Level::DEBUGGING,message,e) @endcode
             * @param message a log message for more info about the exception
             * @param e an exception
             */
         public:
            static void debugging(::std::string message, const ::std::exception& e);

            /**
             * Log a trace message. This method is equivalent to
             * @code log(Level::TRACING,message) @endcode
             * @param message a message
             */
         public:
            static void tracing(const char* message);

            /**
             * Log a trace message. This method is equivalent to
             * @code log(Level::TRACING,message) @endcode
             * @param message a message
             */
         public:
            static void tracing(::std::string message);

            /**
             * Log an exception at the TRACING level. This method is equivalent to
             * @code log(Level::TRACING,message,e) @endcode
             * @param message a log message for more info about the exception
             * @param e an exception
             */
         public:
            static void tracing(::std::string message, const ::std::exception& e);

            /**
             * Log an informational message. This method is equivalent to
             * @code log(Level::INFO,message) @endcode
             * @param message a message
             */
         public:
            static void info(const char* message);

            /**
             * Log an informational message. This method is equivalent to
             * @code log(Level::INFO,message) @endcode
             * @param message a message
             */
         public:
            static void info(::std::string message);

            /**
             * Log an exception at the INFO level. This method is equivalent to
             * @code log(Level::INFO,message,e) @endcode
             * @param message a log message for more info about the exception
             * @param e an exception
             */
         public:
            static void info(::std::string message, const ::std::exception& e);

            /*@}*/

            /**
             * Set or get the current context.
             * @param newContext the new context or nullptr to get the current one
             * @return the context prior to calling this method
             */
         private:
            static ::std::shared_ptr< LogImpl> context(::std::shared_ptr< LogImpl> ctx);

            /**
             * A convenience function.
             * @param newContext the new context or nullptr to get the current one
             * @return the context prior to calling this method
             */
         private:
            static ::std::shared_ptr< LogImpl> context(Log newContext);

            /** The previous log */
         private:
            ::std::shared_ptr< LogImpl> _restore;
      };

   }
}
#endif
