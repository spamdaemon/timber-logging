#include <timber/logging/Formatter.h>
#include <timber/logging/LogRecord.h>
#include <iostream>
#include <iomanip>
#include <ctime>

namespace timber {
   namespace logging {

      Formatter::Formatter()
      {
      }
      Formatter::~Formatter()
      {
      }

      bool Formatter::format(const LogRecord& entry, ::std::ostream& stream)
      {
         formatEntry(entry, stream);
         return true;
      }

      void Formatter::formatTime(const ::timber::logging::Timestamp& entryTime, ::std::ostream& stream)
      {
          auto t = ::std::chrono::system_clock::to_time_t(entryTime);
          tm tm;
          ::gmtime_r(&t, &tm);
          stream << ::std::put_time(&tm, "%F:%T");
      }

      void Formatter::formatLevel(const ::timber::logging::Level& entryLevel, ::std::ostream& stream)
      {
          // emit the log level
           {
              const char* levelName = Level::getName(entryLevel);
              if (!levelName) {
                 // FIXME: keep track of old setw
                 stream << ::std::setw(3) << entryLevel.id();
              }
              else {
                 stream << levelName;
              }
           }
      }
      void Formatter::formatName(const ::std::string& name, ::std::ostream& stream)
      { stream << name; }
      void Formatter::formatLocation(const ::std::string& location, ::std::ostream& stream)
       { stream << location; }
      void Formatter::formatMessage(const ::std::string& message, ::std::ostream& stream)
       { stream << message; }


      void Formatter::formatEntry(const LogRecord& entry, ::std::ostream& stream)
      {
    	 formatTime(entry.time(),stream);
         stream << '|';
         formatLevel(entry.level(),stream);
         stream << '|';
         formatName(entry.logName(),stream);
         stream << '|';
         formatLocation(entry.location(),stream);
         stream << '|';
         formatMessage(entry.message(),stream);
         stream << ::std::endl;
      }

   }
}
