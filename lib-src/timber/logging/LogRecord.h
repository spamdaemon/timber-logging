#ifndef _TIMBER_LOGGING_LOGRECORD_H
#define _TIMBER_LOGGING_LOGRECORD_H

#ifndef _TIMBER_LOGGING_LEVEL_H
#include <timber/logging/Level.h>
#endif

#ifndef _TIMBER_LOGGING_TIMESTAMP_H
#include <timber/logging/Timestamp.h>
#endif

#include <functional>
#include <ostream>
#include <memory>

namespace timber {
   namespace logging {

      /** The log entry factory */
      class LogRecordFactory;

      /**
       * This class provides the implementation for log entries. Currently, the current
       * thread is not part of the LogEntry, but that may be remedied by providing a customized
       * LogEntryFactory. This class is not abstract, but typical application may wish to create
       * subclasses to create customized log entries.
       */
      class LogRecord
      {
            /** The timestamp type */
         public:
            using Timestamp = ::timber::logging::Timestamp;

         private:
            LogRecord(const LogRecord&) = delete;
            LogRecord& operator=(const LogRecord&) = delete;

            /** A function that writes a log message to a stream */
         public:
            typedef ::std::function< void(::std::ostream&)> MessageProducer;

            /** A function that writes a log message to a stream */
         public:
            typedef ::std::function< ::std::string()> LogProducer;

            /**
             * Create a new log entry
             * @param log the name of the log
             * @param lx a log level
             * @param msg the log message
             * @param n the length of the message or -1 if msg is null-terminated
             */
         public:
            LogRecord(::std::string log, Level lx, ::std::string location, const char* msg, ::std::int32_t n = -1);

            /**
             * Create a new log entry
             * @param log the name of the log
             * @param lx a log level
             * @param msg the log message
             */
         public:
            LogRecord(::std::string log, Level lx, ::std::string location, ::std::string msg = ::std::string());

            /** Destructor */
         public:
            virtual ~LogRecord();

            /**
             * Get the log level
             * @return the log level
             */
         public:
            inline Level level() const
            {
               return _level;
            }

            /**
             * Reset the timestamp to the current time.
             */
         public:
            void resetTime();

            /**
             * Get the timestamp.
             * @return the at which this entry was created
             */
         public:
            inline const Timestamp& time() const
            {
               return _time;
            }

            /**
             * Get the name of the log that published this entry.
             * @return the log name
             */
         public:
            inline ::std::string logName() const
            {
               return _log;
            }

            /**
             * Set the message.
             * @param msg a  possibly null-terminated string
             */
         public:
            void setMessage(::std::string msg);

            /**
             * Set the message. If the message pointer is 0, nothing is done.
             * @param msg a  possibly null-terminated string
             * @param n the length of the message string or -1 if it is null-terminated
             */
         public:
            void setMessage(const char* msg, ::std::int32_t n);

            /**
             * Get the message string.
             * @return the message string
             */
         public:
            inline const ::std::string& message() const
            {
               return _message;
            }

            /**
             * Get the location string.
             * @return the location id
             */
         public:
            inline const ::std::string& location() const
            {
               return _location;
            }

            /**
             * Factory methods. If m ==0, then nothing is
             * done.
             * @param m the factory method.
             */
         public:
            static void setFactory(::std::shared_ptr< LogRecordFactory> m);

            /**
             * Create a new log entry.
             * @param log the log
             * @param lx the log level
             * @param location a location or tag for the new record
             * @param producer an initial message producer
             * @return a log entry or 0
             */
         public:
            static ::std::shared_ptr< LogRecord> create(::std::string log, const Level& lx, ::std::string location,
                  const MessageProducer& producer);

            /**
             * Create a new log entry.
             * @param log the log
             * @param lx the log level
             * @param location a location or tag for the new record
             * @param producer an initial message producer
             * @return a log entry or 0
             */
         public:
            static ::std::shared_ptr< LogRecord> create(::std::string log, const Level& lx, ::std::string location,
                  const LogProducer& producer);

            /**
             * Create a new log entry.
             * @param log the log
             * @param lx the log level
             * @param location a location or tag for the new record
             * @param msg the initial message
             * @return a log entry or 0
             */
         public:
            static ::std::shared_ptr< LogRecord> create(::std::string log, const Level& lx, ::std::string location,
                  ::std::string msg);

            /**
             * Create a new log entry. If the message pointer is 0, the 0 is returned!
             * @param log the log
             * @param lx the log level
             * @param location a location or tag for the new record
             * @param msg the initial message
             * @param n the length of the msg or -1 if it's null-terminated
             * @return a log entry or 0
             */
         public:
            static ::std::shared_ptr< LogRecord> create(::std::string log, const Level& lx, ::std::string location,
                  const char* msg, ::std::int32_t n = -1);

            /** The log name */
         private:
            const ::std::string _log;

            /** The  location tag */
         private:
            const ::std::string _location;

            /** The log level */
         private:
            const Level _level;

            /** The creation timestamp */
         private:
            Timestamp _time;

            /** The log message */
         private:
            ::std::string _message;
      };
   }
}

#endif
