#include <timber/logging/StreamLogHandler.h>
#include <iostream>

namespace timber {
   namespace logging {

      StreamLogHandler::StreamLogHandler()
            : _stream(nullptr), _autoflush(false)
      {
      }

      StreamLogHandler::~StreamLogHandler()
      {
      }

      void StreamLogHandler::flush()
      {
         ::std::lock_guard< ::std::mutex> guard(_mutex);
         try {
            ::std::ostream* s = const_cast< ::std::ostream*>(_stream);
            if (s != 0) {
               s->flush();
            }
         }
         catch (const ::std::exception& e) {
            ::std::cerr << __FILE__ << ":" << __LINE__ << ":StreamLogHandler::flush(): " << e.what() << ::std::endl;
         }
         catch (...) {
            ::std::cerr << __FILE__ << ":" << __LINE__ << ":StreamLogHandler::flush()" << ::std::endl;
         }
      }

      void StreamLogHandler::processLogEntry(const ::std::shared_ptr< LogRecord>& x)
      {
         ::std::lock_guard< ::std::mutex> guard(_mutex);
         try {
            ::std::ostream* s = const_cast< ::std::ostream*>(_stream);
            if (s != 0 && s->good()) {
               formatLogEntry(*x, *s);
               if (_autoflush) {
                  s->flush();
               }
            }
         }
         catch (const ::std::exception& e) {
            ::std::cerr << __FILE__ << ":" << __LINE__ << ":StreamLogHandler::processLogEntry(): " << e.what()
                  << ::std::endl;
         }
         catch (...) {
            ::std::cerr << __FILE__ << ":" << __LINE__ << ":StreamLogHandler::processLogEntry()" << ::std::endl;
         }
      }

      void StreamLogHandler::setStream(::std::ostream* s)
      {
         ::std::lock_guard< ::std::mutex> guard(_mutex);
         _stream = s;
      }

   }
}

