#include <bits/exception.h>
#include <timber/logging/Log.h>
#include <timber/logging/LogEntry.h>
#include <timber/logging/LogImpl.h>
#include <timber/logging/LogRecord.h>
#include <algorithm>
#include <cstdint>
#include <iostream>
#include <typeinfo>

namespace timber {
   namespace logging {

      const LogEntryCommit commit = LogEntryCommit();
      const LogEntryCommit doLog = LogEntryCommit();
      const LogEntryCommit endLog = LogEntryCommit();

      LogEntry::LogEntry(const Log& log, Level lev)
            : ::std::ostream(nullptr), _setLog(log._impl), _level(lev),_exceptionType(nullptr)
      {
         ::std::ostream::init(&_buf);
         if (_setLog->isLoggable(lev)) {
            _log = _setLog;
         }
      }

      LogEntry::LogEntry(const Log& log, Level lev, const ::std::exception& ex)
            : ::std::ostream(nullptr), _setLog(log._impl), _level(lev), _exceptionWhat(ex.what()), _exceptionType(&
                  typeid(ex))
      {
         ::std::ostream::init(&_buf);
         if (_setLog->isLoggable(lev)) {
            _log = _setLog;
         }
      }

      LogEntry::LogEntry(const Log& log)

            : LogEntry(log, Level::MIN)
      {
      }

      LogEntry::LogEntry()

            : LogEntry(Log(), Level::MIN)
      {
      }

      LogEntry::LogEntry(Level lev)

            : LogEntry(Log(), lev)
      {
      }

      bool LogEntry::isLoggable(Level logLevel) const
      {
         return _setLog->isLoggable(logLevel);
      }

      LogEntry& LogEntry::reset(const Log& log, Level lev)

      {
         _buf.str(::std::string()); // reset
         _level = lev;
         _setLog = log._impl;
         if (_setLog->isLoggable(lev)) {
            _log = _setLog;
         }
         else {
            _log = nullptr;
         }

         return *this;
      }

      LogEntry& LogEntry::reset(const Log& log, Level lev, const ::std::exception& ex)

      {
         reset(log, lev);
         _exceptionWhat = ex.what();
         _exceptionType = &typeid(ex);
         return *this;
      }

      LogEntry& LogEntry::reset(Level lev)

      {
         _buf.str(::std::string()); // reset
         return level(lev);
      }

      LogEntry& LogEntry::reset(Level lev, const ::std::exception& ex)

      {
         reset(lev);
         _exceptionWhat = ex.what();
         _exceptionType = &typeid(ex);
         return *this;
      }

      LogEntry& LogEntry::location(::std::string loc)
      {
         _location = ::std::move(loc);
         return *this;
      }

      LogEntry& LogEntry::location(const char* loc)
      {
         if (loc) {
            _location = ::std::string(loc);
         }
         else {
            _location.clear();
         }
         return *this;
      }
      LogEntry& LogEntry::location(const char* file, ::std::int32_t line)
      {
         if (file) {
            _location = ::std::string(file);
            _location += ':';
            _location += ::std::to_string(line);
         }
         else {
            // ignore line in this case
            _location.clear();
         }
         return *this;
      }
      LogEntry& LogEntry::exception(const ::std::exception& ex)
      {
         _exceptionWhat = ex.what();
         _exceptionType = &typeid(ex);
         return *this;
      }

      LogEntry& LogEntry::level(Level lev)
      {
         _level = lev;
         if (_setLog->isLoggable(lev)) {
            _log = _setLog;
         }
         else {
            _log = nullptr;
         }

         return *this;
      }

      void LogEntry::commit()

      {
         if (_log != nullptr) {

            // first, add some formatting to make the string look as if it was published with the normal Log
            if (_exceptionType) {
               *this << " : " << _exceptionType->name() << " : " << _exceptionWhat;
            }

            ::std::shared_ptr< LogRecord> e(LogRecord::create(_log->name(), _level, _location, _buf.str()));
            if (e) {
               _log->log(e);
            }
            else {
               // there is not much we can do; the log entry could not be created
               ::std::cerr << "LOGGING ERROR: could not create LogEntry for " << _log->name() << " : " << _buf.str()
                     << ::std::endl;
            }
         }
         // clear the buffer and get it ready for more logging
         _buf.str(::std::string());
      }

   }

}

void operator<<(::std::ostream& out, const ::timber::logging::LogEntryCommit&)
{
   timber::logging::LogEntry* e = dynamic_cast< timber::logging::LogEntry*>(&out);
   if (e) {
      e->commit();
   }
   else {
      // applying commit to a stream that is not a log stream; ignore it.
   }
}

::std::ostream& operator<<(::std::ostream& stream, const ::std::type_info& info)
{
	return stream << info.name();
}

