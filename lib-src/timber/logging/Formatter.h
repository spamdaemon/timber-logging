#ifndef _TIMBER_LOGGING_FORMATTER_H
#define _TIMBER_LOGGING_FORMATTER_H

#ifndef _TIMBER_LOGGING_TIMESTAMP_H
#include <timber/logging/Timestamp.h>
#endif

#include <iosfwd>
#include <string>

namespace timber {
   namespace logging {

      /** A log entry */
      class LogRecord;
      class Level;

      /**
       * The Formatter is used to write a formatted version of a log entry
       * to an output stream. A default formatting function exists, which can
       * format any LogRecord.
       */
      class Formatter
      {
            Formatter(const Formatter &) = delete;
            Formatter & operator=(const Formatter &) = delete;

            /** Default constructor  */
         public:
            Formatter();

            /** The destructor */
         protected:
            virtual ~Formatter();

            /**
             * Format the specified log entry and write it to
             * the specified output stream. Callers should check
             * the stream for any errors. The default implementation
             * calls formatEntry() to do the actual formatting.
             *
             * @param entry a log entry
             * @param stream an output stream
             * @return true if the entry was written to the stream, false otherwise
             */
         public:
            virtual bool format(const LogRecord& entry, ::std::ostream& stream);

            /**
             * Format an entry. This method will always succeed.
             * @param entry a log entry
             * @param stream an output stream
             */
         public:
            static void formatEntry(const LogRecord& entry, ::std::ostream& stream);

            /**
             * Format the time of a log entry.
             * @parm entryTime the time of a log entry
             * @param stream an output stream
             */
         public:
            static void formatTime(const ::timber::logging::Timestamp& entryTime, ::std::ostream& stream);

            /**
             * Format a log level
             * @param entryLevel the log entry level
             * @param stream an output stream
             */
         public:
            static void formatLevel(const ::timber::logging::Level& entryLevel, ::std::ostream& stream);

            /**
             * Format name of a log entry.
             * @param name the name of a log entry
             * @param stream an output stream
             */
          public:
            static void formatName(const ::std::string& name, ::std::ostream& stream);

            /**
             * Format a log level
             * @param location the location information
             * @param stream an output stream
             */
          public:
            static void formatLocation(const ::std::string& location, ::std::ostream& stream);

             /** Format a log level
               * @param message the log message
               * @param stream an output stream
               */
           public:
             static void formatMessage(const ::std::string& message, ::std::ostream& stream);

      };

   }
}

#endif
