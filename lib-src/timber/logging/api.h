#ifndef _TIMBER_LOGGING_API_H
#define _TIMBER_LOGGING_API_H

#ifndef _TIMBER_LOGGING_H
#include <timber/logging.h>
#endif

#ifndef _TIMBER_LOGGING_REGISTRY_H
#include <timber/logging/Registry.h>
#endif

#ifndef _TIMBER_LOGGING_FORMATTER_H
#include <timber/logging/Formatter.h>
#endif

#ifndef _TIMBER_LOGGING_LOGHANDLER_H
#include <timber/logging/LogHandler.h>
#endif

#ifndef _TIMBER_LOGGING_LOGIMPL_H
#include <timber/logging/LogImpl.h>
#endif

#endif
