#ifndef _TIMBER_LOGGING_LOGENTRY_H
#define _TIMBER_LOGGING_LOGENTRY_H

#ifndef _TIMBER_LOGGING_LEVEL_H
#include <timber/logging/Level.h>
#endif

#include <string>
#include <iosfwd>
#include <memory>
#include <sstream>
#include <iostream>
#include <typeinfo>

namespace timber {
   namespace logging {
      class Log;
      class LogImpl;

      /**
       * Log entries are the actual entities being created when a message is logged. The LogEntry allows clients
       * full access to that internal datastructure, which is mostly hidden by the simple Log::log... methods.
       * One of the key strengths of this class is that the full set of C++ stream operators can be used.
       *
       * @note Some notes when using this class:
       * <ol>
       * <li>Since the use of the LogEntry class is usually wordier than those of the Log class, the latter should be
       * preferred for simple log messages.
       * <li>Due to a peculiarity in C++ language, the following construct does not log the intended string, but rather
       * the pointer to the string:
       * <pre>
       *   LogEntry(Level::DEBUGGING) << "A debug message" << commit;
       * </pre>
       * Use this work-around to obtain the desired result:
       * <pre>
       *   LogEntry(Level::DEBUGGING).stream() << "A debug message" << commit;
       * </pre>
       * </ol>
       */
      class LogEntry : public ::std::ostream
      {
         private:
            LogEntry(const LogEntry&) = delete;
            LogEntry& operator=(const LogEntry&) = delete;

            /**
             * Create a new log entry whose level is set to Level::MIN.
             * @param log a log
             */
         public:
            LogEntry(const Log& log);

            /**
             * Create a new log entry in the global log with log level
             * of Level::MIN
             */
         public:
            LogEntry();

            /**
             * Create a new log entry.
             * @param log a log
             * @param lev a log lev
             */
         public:
            LogEntry(const Log& log, Level lev);

            /**
             * Create a new log entry.
             * @param log a log
             * @param lev a log lev'
             * @param ex an exception
             */
         public:
            LogEntry(const Log& log, Level lev, const ::std::exception& ex);

            /**
             * Create a new log entry in the global log.
             * @param lev a log level
             */
         public:
            LogEntry(Level lev);

            /**
             * Determine if entries at the specified log level will be committed to this log.
             * If the level.compare(level())>0 then this method must return false.
             * @param logLevel a log level
             * @return true if the entries will be logged at the specified level
             */
         public:
            bool isLoggable(Level logLevel) const;

            /**
             * Commit this log entry to its associated log. If this
             * method is invoked more than once, then all but the first
             * invocation will be ignored.
             */
         public:
            void commit();

            /**
             * Get the log stream. The same effect can be achieved by
             * calling flush().
             * @return the stream to which this log entry will be written
             */
         public:
            inline LogEntry& stream()
            {
               return *this;
            }

            /**
             * Reset the log-level if possible, and get the log stream.
             * Any uncommitted data is lost.
             * @param log the new log
             * @param lev the new log level
             * @return the stream to which this log entry will be written
             */
         public:
            LogEntry& reset(const Log& log, Level lev);

            /**
             * Reset the log-level if possible, and get the log stream.
             * Any uncommitted data is lost.
             * @param log the new log
             * @param lev the new log level
             * @param ex the exception involved
             * @return the stream to which this log entry will be written
             */
         public:
            LogEntry& reset(const Log& log, Level lev, const ::std::exception& ex);

            /**
             * Reset the log-level if possible, and get the log stream.
             * Any uncommitted data is lost.
             * @param lev the new log level
             * @return the stream to which this log entry will be written
             */
         public:
            LogEntry& reset(Level lev);

            /**
             * Reset the log-level if possible, and get the log stream.
             * Any uncommitted data is lost.
             * @param lev the new log level
             * @param ex the exception involved
             * @return the stream to which this log entry will be written
             */
         public:
            LogEntry& reset(Level lev, const ::std::exception& ex);

            /**
             * Change the level of the current log. Uncommited data is not lost.
             * @param lev the new log level
             * @return the stream to which this log entry will be written
             */
         public:
            LogEntry& level(Level lev);

            /**
             * Change the location of the current log. Uncommited data is not lost.
             * The location can be used as a free-form text field that is normally used to indicate
             * file location where the log was made.
             * @param loc the new location
             * @return the stream to which this log entry will be written
             */
         public:
            LogEntry& location(::std::string loc);

            /**
             * Change the location of the current log. Uncommited data is not lost.
             * @param location the new location
             * @return the stream to which this log entry will be written
             */
         public:
            LogEntry& location(const char* loc);

            /**
             * Change the location of the current log to the specified file and line.
             * This method specifically exists for calls with __FILE__ and __LINE__.
             * @param file a file name
             * @param line a line number
             * @return the stream to which this log entry will be written
             */
         public:
            LogEntry& location(const char* file, ::std::int32_t line);

            /**
             * Replace the current exception information.
             * @param ex an exception object
             */
         public:
            LogEntry& exception(const ::std::exception& e);

            /**
             * Get the stream and reset the log-level to throwing.
             * @return the stream to which this log entry will be written
             */
         public:
            inline LogEntry& throwing(const Log& log)
            {
               return reset(log, Level::THROWING);
            }

            /**
             * Get the stream and reset the log-level to throwing.
             * @param ex the exception that will be thrown
             * @return the stream to which this log entry will be written
             */
         public:
            inline LogEntry& throwing(const Log& log, const ::std::exception& ex)
            {
               return reset(log, Level::THROWING, ex);
            }

            /**
             * Get the stream and reset the log-level to caught.
             * @return the stream to which this log entry will be written
             */
         public:
            inline LogEntry& caught(const Log& log)
            {
               return reset(log, Level::CAUGHT);
            }

            /**
             * Get the stream and reset the log-level to caught.
             * @param ex the exception caught
             * @return the stream to which this log entry will be written
             */
         public:
            inline LogEntry& caught(const Log& log, const ::std::exception& ex)
            {
               return reset(log, Level::CAUGHT, ex);
            }

            /**
             * Get the stream and reset the log-level to info.
             * @return the stream to which this log entry will be written
             */
         public:
            inline LogEntry& info(const Log& log)
            {
               return reset(log, Level::INFO);
            }

            /**
             * Get the stream and reset the log-level to tracing.
             * @return the stream to which this log entry will be written
             */
         public:
            inline LogEntry& tracing(const Log& log)
            {
               return reset(log, Level::TRACING);
            }

            /**
             * Get the stream and reset the log-level to debugging.
             * @return the stream to which this log entry will be written
             */
         public:
            inline LogEntry& debugging(const Log& log)
            {
               return reset(log, Level::DEBUGGING);
            }

            /**
             * Get the stream and reset the log-level to exception.
             * @return the stream to which this log entry will be written
             */
         public:
            inline LogEntry& exception(const Log& log)
            {
               return reset(log, Level::EXCEPTION);
            }

            /**
             * Get the stream and reset the log-level to warn.
             * @return the stream to which this log entry will be written
             */
         public:
            inline LogEntry& warn(const Log& log)
            {
               return reset(log, Level::WARN);
            }

            /**
             * Get the stream and reset the log-level to severe.
             * @return the stream to which this log entry will be written
             */
         public:
            inline LogEntry& severe(const Log& log)
            {
               return reset(log, Level::SEVERE);
            }

            /**
             * Get the stream and reset the log-level to bug.
             * @return the stream to which this log entry will be written
             */
         public:
            inline LogEntry& bug(const Log& log)
            {
               return reset(log, Level::BUG);
            }

            /**
             * Get the stream and reset the log-level to all.
             * @return the stream to which this log entry will be written
             */
         public:
            inline LogEntry& all(const Log& log)
            {
               return reset(log, Level::ALL);
            }

            /**
             * Get the stream and reset the log-level to throwing.
             * @return the stream to which this log entry will be written
             */
         public:
            inline LogEntry& throwing()
            {
               return reset(Level::THROWING);
            }

            /**
             * Get the stream and reset the log-level to throwing.
             * @param ex the exception that will be thrown.
             * @return the stream to which this log entry will be written
             */
         public:
            inline LogEntry& throwing(const ::std::exception& ex)
            {
               return reset(Level::THROWING, ex);
            }

            /**
             * Get the stream and reset the log-level to caught.
             * @return the stream to which this log entry will be written
             */
         public:
            inline LogEntry& caught()
            {
               return reset(Level::CAUGHT);
            }

            /**
             * Get the stream and reset the log-level to caught.
             * @param the exception caught
             * @return the stream to which this log entry will be written
             */
         public:
            inline LogEntry& caught(const ::std::exception& ex)
            {
               return reset(Level::CAUGHT, ex);
            }

            /**
             * Get the stream and reset the log-level to info.
             * @return the stream to which this log entry will be written
             */
         public:
            inline LogEntry& info()
            {
               return reset(Level::INFO);
            }

            /**
             * Get the stream and reset the log-level to tracing.
             * @return the stream to which this log entry will be written
             */
         public:
            inline LogEntry& tracing()
            {
               return reset(Level::TRACING);
            }

            /**
             * Get the stream and reset the log-level to tracing.
             * @return the stream to which this log entry will be written
             */
         public:
            inline LogEntry& debugging()
            {
               return reset(Level::DEBUGGING);
            }

            /**
             * Get the stream and reset the log-level to exception.
             * @return the stream to which this log entry will be written
             */
         public:
            inline LogEntry& exception()
            {
               return reset(Level::EXCEPTION);
            }

            /**
             * Get the stream and reset the log-level to warn.
             * @return the stream to which this log entry will be written
             */
         public:
            inline LogEntry& warn()
            {
               return reset(Level::WARN);
            }

            /**
             * Get the stream and reset the log-level to severe.
             * @return the stream to which this log entry will be written
             */
         public:
            inline LogEntry& severe()
            {
               return reset(Level::SEVERE);
            }

            /**
             * Get the stream and reset the log-level to bug.
             * @return the stream to which this log entry will be written
             */
         public:
            inline LogEntry& bug()
            {
               return reset(Level::BUG);
            }

            /**
             * Get the stream and reset the log-level to all.
             * @return the stream to which this log entry will be written
             */
         public:
            inline LogEntry& all()
            {
               return reset(Level::ALL);
            }

            /** The string buffer */
         private:
            ::std::stringbuf _buf;

            /** The effective log or null */
         private:
            ::std::shared_ptr< LogImpl> _log;

            /** The log or null */
         private:
            ::std::shared_ptr< LogImpl> _setLog;

            /** The log level */
         private:
            Level _level;

            /** The location */
         private:
            ::std::string _location;

            /** The exception's what string, or an empty string if no exception was specified */
         private:
            ::std::string _exceptionWhat;

            /**
             * The exception's type. typeid has static storage duration,
             * but there could be issues at program exit.
             * */
         private:
            const ::std::type_info* _exceptionType;
      };

      /**
       * This class is just a dummy type that will allow us to commit a log entry to
       * the intended log.
       */
      struct LogEntryCommit
      {
      };

      /**
       * An external object which must be used to commit a log. Use it like this:
       * <pre>
       * LogEntry(Level::INFO).stream() << "An informational message" << commit;
       * </pre>
       */
      extern const LogEntryCommit commit;

      /**
       * An external object which must be used to commit a log. Use it like this:
       * <pre>
       * LogEntry(Level::INFO).stream() << "An informational message" << doLog;
       * </pre>
       */
      extern const LogEntryCommit doLog;

      /**
       * An external object which must be used to commit a log. Use it like this:
       * <pre>
       * LogEntry(Level::INFO).stream() << "An informational message" << doLog;
       * </pre>
       */
      extern const LogEntryCommit endLog;
   }
}

/**
 * Commit a log entry. Commits a log entry if the specified
 * is a LogEntry, otherwise does nothing.
 * @note this method purposefully does not return anything!
 * @param stream a stream or log entry
 */
void operator<<(::std::ostream& stream, const ::timber::logging::LogEntryCommit&);

/**
 * Log a type info.
 * @param stream a stream or log entry
 * @param info the type information to log
 */
::std::ostream& operator<<(::std::ostream& stream, const ::std::type_info& info);

#endif
