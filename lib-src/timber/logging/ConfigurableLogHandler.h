#ifndef _TIMBER_LOGGING_CONFIGURABLELOGHANDLER_H
#define _TIMBER_LOGGING_CONFIGURABLELOGHANDLER_H

#ifndef _TIMBER_LOGGING_LOGHANDLER_H
#include <timber/logging/LogHandler.h>
#endif

#ifndef _TIMBER_LOGGING_FORMATTER_H
#include <timber/logging/Formatter.h>
#endif

namespace timber {
   namespace logging {

      /**
       * This log handler allows clients to set a custom formatter. This class is still abstract
       * and clients must implement the processLogEntry() method.
       */
      class ConfigurableLogHandler : public LogHandler
      {
            /** @name Disable copying
             * @{
             */
         private:
            ConfigurableLogHandler(const ConfigurableLogHandler&) = delete;
            ConfigurableLogHandler& operator=(const ConfigurableLogHandler&) = delete;
            /*@}*/

            /** Default constructor */
         protected:
            ConfigurableLogHandler();

            /** Destructor */
         protected:
            ~ConfigurableLogHandler();

            /**
             * Set the formatter for this handler. Subclasses may override this
             * and are not required to return true.
             * @param fmt a formatter or 0 to use a default formatter
             * @return true
             */
         public:
            bool setFormatter(const ::std::shared_ptr< Formatter>& fmt);

            /**
             * Format the specified log entry using the currently set formatter.
             * @param entry a log entry
             * @param out an output
             */
         protected:
            void formatLogEntry(const LogRecord& entry, ::std::ostream& out);

            /** The formatter */
         private:
            ::std::shared_ptr< Formatter> _formatter;
      };

   }
}

#endif
