#include <timber/logging/ConfigurableLogHandler.h>
#include <timber/logging/Formatter.h>
#include <timber/logging/LogRecord.h>

namespace timber {
   namespace logging {

      ConfigurableLogHandler::ConfigurableLogHandler()
      {
      }

      ConfigurableLogHandler::~ConfigurableLogHandler()
      {
      }

      void ConfigurableLogHandler::formatLogEntry(const LogRecord& entry, ::std::ostream& out)
      {
         ::std::shared_ptr< Formatter> fmt(_formatter);
         if (!fmt) {
            Formatter::formatEntry(entry, out);
         }
         else {
            fmt->format(entry, out);
         }
      }

      bool ConfigurableLogHandler::setFormatter(const ::std::shared_ptr< Formatter>& fmt)
      {
         _formatter = fmt;
         return true;
      }

   }
}
