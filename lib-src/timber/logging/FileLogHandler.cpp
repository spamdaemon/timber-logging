#include <timber/logging/FileLogHandler.h>
#include <fstream>
#include <stdexcept>

namespace timber {
   namespace logging {

      FileLogHandler::FileLogHandler(const ::std::string& name)
            : _stream(new ::std::ofstream(name.c_str(), ::std::ios_base::ate | ::std::ios_base::out))
      {
         if (_stream->good()) {
            StreamLogHandler::setStream(_stream.get());
         }
         else {
            throw ::std::runtime_error(::std::string("Could not open file ") + name);
         }
      }

      FileLogHandler::FileLogHandler(const ::std::string& name, bool append)
      {
         if (append) {
            _stream.reset(new ::std::ofstream(name.c_str()));
         }
         else {
            _stream.reset(new ::std::ofstream(name.c_str(), ::std::ios_base::ate | ::std::ios_base::out));
         }
         if (_stream->good()) {
            StreamLogHandler::setStream(_stream.get());
         }
         else {
            throw ::std::runtime_error(::std::string("Could not open file ") + name);
         }
      }

      FileLogHandler::~FileLogHandler()
      {
         StreamLogHandler::setStream(0);
         // cause the stream to close
         _stream.reset(0);
      }

   }
}

