#ifndef _TIMBER_LOGGING_LOG_H
#define _TIMBER_LOGGING_LOG_H

#ifndef _TIMBER_LOGGING_LOGIMPL_H
#include <timber/logging/LogImpl.h>
#endif

#ifndef _TIMBER_LOGGING_LEVEL_H
#include <timber/logging/Level.h>
#endif

#ifndef _TIMBER_LOGGING_LOGRECORD_H
#include <timber/logging/LogRecord.h>
#endif

#include <string>
#include <functional>
#include <ostream>

namespace timber {
   namespace logging {

      class LogEntry;
      class LogContext;

      /**
       * The Log is the class through which applications send their log messages
       * to the logging subsystem. The Log class itself is merely a convenience
       * wrapper for the LogImpl class which provides the actual logging. An
       * instance of the LogImpl always exists and is accessible via either the
       * Log() default constructor, or the rootLog() method. The Log class work
       * in concert with the Registry to find preconfigured LogImpl instances.
       * Clients may obtain instances of specific logs through either one of the
       * Log(const char*) or Log(const ::std::string&) constructors, or directly
       * via one of the Registry::findLog() methods. Furthermore, clients may create
       * their own Logs by subclassing LogImpl. Hierarchical logging is supported
       * via the create() method, which creates a log whose configuration is based
       * on the current log, but which may be modified without modifying the current
       * log.
       * <br>
       * Configuration of logs can accomplished by adding or removing various
       * LogHandler objects. A LogHandler is provides that actual logging
       * of the LogEntries to a file, network, etc.
       * <br>
       * Filtering of log message based on a log level is accomplished by through
       * the setLevel() method. By setting a specific log level, all message logged
       * at or below that level will be passed on to the log's handlers. Those message
       * logged above the current log level will be ignored. In this example,
       * @code
       *   Log log;
       *   log.setLevel(Level::INFO);
       *   log.warn("A warning");
       *   log.info("A some info");
       *   log.debug("A some debug info");
       * @endcode
       * the warning and info messages will be logged, but the debug message will not.
       * <p>
       * Logs can be configured in the Application configuration. To configure the loglevel
       * of a log called X use this property @code X.log.level" @endcode.
       *
       * @note The use of the logging facilities is completely thread-safe.
       */
      class Log
      {
            /** The log entry is a friend */
            friend class LogEntry;

            /** The log entry is a friend */
            friend class LogContext;

            /** The root log name */
         public:
            static const char ROOT_LOG_NAME[];

            /**
             * A function that writes a log message to a stream. The message producer
             * will be used to generate the log message if a log entry is created.
             * The will not be used after the method calls return and thus use local
             * stack variables.
             */
         public:
            typedef ::std::function< void(::std::ostream&)> MessageProducer;

            /**
             * A function that creates a string that will be logged. The function
             * is only executed when a log is actually created.
             */
         public:
            typedef ::std::function< ::std::string()> LogProducer;

            /**
             * Create the root log.
             */
         public:
            Log();

             /**
             * Get a log by the specified name or create one if necessary.
             * Uses Registry::findLog(name,true)
             * @param name a log name or nullptr to get the root log.
             * @throws ::std::exception if the log could not be accessed
             */
         public:
            Log(const char* name);

            /**
             * Uses Registry::find(name,true)
             * @param name a log name
             * @throws ::std::exception if the log could not be accessed
             */
         public:
            Log(const ::std::string& name);

            /**
             * Create a log using the specified implementation.
             * @param impl a log implementation
             * @throws ::std::exception if the log could not be accessed
             * @throws ::std::invalid_argument if impl==nullptr
             */
         public:
            Log(::std::shared_ptr< LogImpl> impl);

            /**
             * Create a child of this log. If the name is the empty
             * string, then an anonymous log will be created.
             * @param name the desired name of the new log.
             * @param level the initial level
             * @return a log based on this one
             * @throws ::std::exception if the log could not be created
             */
         public:
            Log create(const ::std::string& name, Level level) const;

            /**
             * Create a child of this log. If the name is the empty
             * string, then an anonymous log will be created. The log
             * level will be inherited.
             * @param name the desired name of the new log.
             * @return a log based on this one
             * @throws ::std::exception if the log could not be created
             */
         public:
            Log create(const ::std::string& name) const;

            /**
             * Get the root log. This is equivalen to @code return Log(ROOT_LOG_NAME) @endcode.
             * @return the root log
             * @throws ::std::exception if the rootLog() does not work
             */
         public:
            static Log rootLog();

            /**
             * Get the name of this log.
             * @return a string that is the name of this log
             */
         public:
            ::std::string name() const;

            /**
             * Add a new handler to this log.
             * @param handler a log handler
             * @throws invalid_argument if the handler has already been added
             */
         public:
            void addLogHandler(::std::shared_ptr< LogHandler> handler);

            /**
             * Remove all handlers from this log.
             */
         public:
            void removeLogHandlers();

            /**
             * Remove a handler from this log. It is not an error if the log
             * handler has not been added to this log.
             * @param handler a log handler.
             */
         public:
            void removeLogHandler(::std::shared_ptr< LogHandler> handler);

            /**
             * Set the log level.
             * @param newLevel the new log level
             */
         public:
            void setLevel(const Level& newLevel);

            /**
             * Get the current log level. This method will affect
             * whether or not isLoggable() returns true, but it does not
             * imply that isLoggable() always returns true for a log level
             * below the current log level.
             * @return the current log level
             */
         public:
            Level level() const;

            /**
             * Determine if entries at the specified log level will be committed to this log.
             * If the level.compare(level())>0 then this method must return false.
             * @param logLevel a log level
             * @return true if the entries will be logged at the specified level
             * @note a logLevel of Level::OFF is never logged!
             */
         public:
            inline bool isLoggable(const Level& logLevel) const
            {
               return _level->isLoggable(logLevel);
            }

            /**
             * @name Various functions that allow logging at different or predefined levels.
             * @{
             */

            /**
             * Log the specified entry. If @code e==0 @endcode
             * then nothing done.
             * @note It is no error if the entry's logname does not match this log's name!
             * @param e a log entry or 0
             */
         public:
            void log(::std::shared_ptr< LogRecord> e);

            /**
             * Log the specified entry.
             * @note It is no error if the entry's logname does not match this log's name!
             * @param e a log entry
             */
         public:
            void log(::std::unique_ptr< LogRecord> e);

            /**
             * Log a message.
             * @param logLevel the log level
             * @param message the log message
             */
         public:
            void log(const Level& logLevel, ::std::string message);

            /**
             * Log a message. If message is 0, then nothing is logged. Most of the time, the
             * check for @code message!=0 @endcode should be optimized away by the compiler since
             * the string is probably a string literal.
             * @param logLevel the log level
             * @param message the log message
             */
         public:
            void log(const Level& logLevel, const char* message);

            /**
             * Log a message at the given level. The message is produced by the provided
             * function.
             * @param logLevel the level at which to log
             * @param producer the message producing function
             */
         public:
            void log(const Level& logLevel, const MessageProducer& producer);

            /**
             * Log a message at the given level. The message is produced by the provided
             * function.
             * @param logLevel the level at which to log
             * @param producer the message producing function
             */
         public:
            void log(const Level& logLevel, const LogProducer& producer);

            /**
             * Log an exception.
             * @param logLevel the log level
             * @param message a log message for more info about the exception
             * @param ex an exception
             */
         public:
            void log(const Level& logLevel, ::std::string message, const ::std::exception& ex);

            /**
             * Log an exception that is being thrown. This method is equivalent to
             * @code log(Level::THROWING,message,e) @endcode.
             * @param message a log message for more info about the exception
             * @param ex an exception
             */
         public:
            void throwing(::std::string message, const ::std::exception& ex);

            /**
             * Log an exception that is being thrown. This method is equivalent to
             * @code log(Level::THROWING,"Throwing exception",e) @endcode.
             * @param ex an exception
             */
         public:
            void throwing(const ::std::exception& ex);

            /**
             * Log an exception that has been caught. This method is equivalent to
             * @code log(Level::CAUGHT,message,e) @endcode.
             * @param message a log message for more info about the exception
             * @param ex an exception
             */
         public:
            void caught(::std::string message, const ::std::exception& ex);

            /**
             * Log an exception that has been caught. This method is equivalent to
             * @code log(Level::CAUGHT,"Caught exception",e) @endcode.
             * @param ex an exception
             */
         public:
            void caught(const ::std::exception& ex);

            /**
             * Log that an unexpected exception was caught. This is typically a severe problem
             * and is thus logged at the Level::SEVERE level and not the Level::CAUGHT level as the
             * name of the method implies.
             * @code log(Level::SEVERE,"Caught unexpected exception") @endcode.
             * @param ex an exception
             */
         public:
            void caughtUnexpected();

            /**
             * Log a warning. This method is equivalent to
             * @code log(Level::WARN,message) @endcode
             * @param producer a message producer
             */
         public:
            void warn(const MessageProducer& producer);

            /**
             * Log a warning. This method is equivalent to
             * @code log(Level::WARN,message) @endcode
             * @param producer a message producer
             */
         public:
            void warn(const LogProducer& producer);

            /**
             * Log a warning. This method is equivalent to
             * @code log(Level::WARN,message) @endcode
             * @param message a message
             */
         public:
            void warn(const char* message);

            /**
             * Log a warning. This method is equivalent to
             * @code log(Level::WARN,message) @endcode
             * @param message a message
             */
         public:
            void warn(::std::string message);

            /**
             * Log an exception at the warning level. This method is equivalent to
             * @code log(Level::WARN,message,e) @endcode
             * @param message a log message for more info about the exception
             * @param e an exception
             */
         public:
            void warn(::std::string message, const ::std::exception& e);

            /**
             * Log a warning. This method is equivalent to
             * @code log(Level::WARN,message) @endcode
             * @param producer a message producer
             */
         public:
            void severe(const MessageProducer& producer);

            /**
             * Log a warning. This method is equivalent to
             * @code log(Level::WARN,message) @endcode
             * @param producer a message producer
             */
         public:
            void severe(const LogProducer& producer);

            /**
             * Log a severe error message. This method is equivalent to
             * @code log(Level::SEVERE,message) @endcode
             * @param message a message
             */
         public:
            void severe(const char* message);

            /**
             * Log a severe error message. This method is equivalent to
             * @code log(Level::SEVERE,message) @endcode
             * @param message a message
             */
         public:
            void severe(::std::string message);

            /**
             * Log a severe exception. This method is equivalent to
             * @code log(Level::SEVERE,message,e) @endcode
             * @param message a log message for more info about the exception
             * @param e an exception
             */
         public:
            void severe(::std::string message, const ::std::exception& e);

            /**
             * Log a bug message. This method is equivalent to
             * @code log(Level::BUG,message) @endcode
             * @param producer a message producer
             */
         public:
            void bug(const MessageProducer& producer);

            /**
             * Log a bug message. This method is equivalent to
             * @code log(Level::BUG,message) @endcode
             * @param producer a message producer
             */
         public:
            void bug(const LogProducer& producer);

            /**
             * Log a bug message. This method is equivalent to
             * @code log(Level::BUG,message) @endcode
             * @param message a message
             */
         public:
            void bug(const char* message);

            /**
             * Log a bug message. This method is equivalent to
             * @code log(Level::BUG,message) @endcode
             * @param message a message
             */
         public:
            void bug(::std::string message);

            /**
             * Log a bug message. This method is equivalent to
             * @code log(Level::BUG,message,e) @endcode
             * @param message a log message for more info about the exception
             * @param e an exception
             */
         public:
            void bug(::std::string message, const ::std::exception& e);

            /**
             * Log a debugging message. This method is equivalent to
             * @code log(Level::DEBUGGING,message) @endcode
             * @param producer a message producer
             */
         public:
            void debugging(const MessageProducer& producer);

            /**
             * Log a debugging message. This method is equivalent to
             * @code log(Level::DEBUGGING,message) @endcode
             * @param producer a message producer
             */
         public:
            void debugging(const LogProducer& producer);

            /**
             * Log a debug message. This method is equivalent to
             * @code log(Level::DEBUGGING,message) @endcode
             * @param message a message
             */
         public:
            void debugging(const char* message);

            /**
             * Log a debug message. This method is equivalent to
             * @code log(Level::DEBUGGING,message) @endcode
             * @param message a message
             */
         public:
            void debugging(::std::string message);

            /**
             * Log an exception at the DEBUGGING level. This method is equivalent to
             * @code log(Level::DEBUGGING,message,e) @endcode
             * @param message a log message for more info about the exception
             * @param e an exception
             */
         public:
            void debugging(::std::string message, const ::std::exception& e);

            /**
             * Log a tracing message. This method is equivalent to
             * @code log(Level::TRACING,message) @endcode
             * @param producer a message producer
             */
         public:
            void tracing(const MessageProducer& producer);

            /**
             * Log a tracing message. This method is equivalent to
             * @code log(Level::TRACING,message) @endcode
             * @param producer a message producer
             */
         public:
            void tracing(const LogProducer& producer);

            /**
             * Log a trace message. This method is equivalent to
             * @code log(Level::TRACING,message) @endcode
             * @param message a message
             */
         public:
            void tracing(const char* message);

            /**
             * Log a tracing message. This method is equivalent to
             * @code log(Level::TRACING,message) @endcode
             * @param message a message
             */
         public:
            void tracing(::std::string message);

            /**
             * Log an exception at the TRACING level. This method is equivalent to
             * @code log(Level::TRACING,message,e) @endcode
             * @param message a log message for more info about the exception
             * @param e an exception
             */
         public:
            void tracing(::std::string message, const ::std::exception& e);

            /**
             * Log a info message. This method is equivalent to
             * @code log(Level::INFO,message) @endcode
             * @param producer a message producer
             */
         public:
            void info(const MessageProducer& producer);

            /**
             * Log a info message. This method is equivalent to
             * @code log(Level::INFO,message) @endcode
             * @param producer a message producer
             */
         public:
            void info(const LogProducer& producer);

            /**
             * Log an informational message. This method is equivalent to
             * @code log(Level::INFO,message) @endcode
             * @param message a message
             */
         public:
            void info(const char* message);

            /**
             * Log an informational message. This method is equivalent to
             * @code log(Level::INFO,message) @endcode
             * @param message a message
             */
         public:
            void info(::std::string message);

            /**
             * Log an exception at the INFO level. This method is equivalent to
             * @code log(Level::INFO,message,e) @endcode
             * @param message a log message for more info about the exception
             * @param e an exception
             */
         public:
            void info(::std::string message, const ::std::exception& e);

            /*@}*/

            /**
             * Log a message. This method is used internally and will log whenever the
             * level is loggable.
             * @param logLevel the log level
             * @param message the log message
             */
         private:
            void logInternal(const Level& logLevel, ::std::string message);

            /**
             * Log a message. This method is used internally and will log whenever the
             * level is loggable.
             * @param logLevel the log level
             * @param message the log message
             */
         private:
            void logInternal(const Level& logLevel, const char* message);

            /**
             * Log a message. This method is used internally and will log whenever the
             * level is loggable.
             * @param logLevel the log level
             * @param message a message with more details about the exception
             * @param e an exception to be logged
             */
         private:
            void logInternal(const Level& logLevel, ::std::string message, const ::std::exception& e);

            /**
             * Log a message at the given level. The message is produced by the provided
             * function.
             * @param logLevel the level at which to log
             * @param producer the message producing function
             */
         public:
            void logInternal(const Level& logLevel, const MessageProducer& producer);

            /**
             * Log a message at the given level. The message is produced by the provided
             * function.
             * @param logLevel the level at which to log
             * @param producer the message producing function
             */
         public:
            void logInternal(const Level& logLevel, const LogProducer& producer);

            /** The log level (will never be nullptr) */
         private:
            ::std::shared_ptr< const LogLevel> _level;

            /** The implementation */
         private:
            ::std::shared_ptr< LogImpl> _impl;
      }
      ;
   }
}

#endif
