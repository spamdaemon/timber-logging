#ifndef _TIMBER_LOGGING_LEVEL_H
#define _TIMBER_LOGGING_LEVEL_H

#include <stdexcept>
#include <cinttypes>
#include <string>

namespace timber {
   namespace logging {

      /**
       * The log level defines the priority with which messages are logged. When setting the log
       * level for a log, a message at or below the priority level will be logged. This implies
       * that lower log levels have higher priority. For instance, a log which has a
       * log level of 0, will accept messages logged at levels -128 to 0, inclusive.
       * <br>
       * There are 256 log levels ranging from -128 to 127. A set of suggested log levels is given
       * by DEBUGGING, TRACING, INFO, EXCEPTION, CAUGHT, THROWING, WARN, SEVERE, BUG.
       */
      class Level
      {

            /**
             * Log the maximum number of messages. Using this level
             * only makes sense when setting the level of a log entry.
             */
         public:
            static const Level MAX;

            /**
             * Log the minimum number of messages. Using this level
             * only makes sense when setting the level of a log entry.
             */
         public:
            static const Level MIN;

            /**
             * Logs all everything. Be careful when using this level for a log message,
             * because it will only be logged if the log level is set to this value!
             */
         public:
            static const Level ALL;

            /** Log debugging information */
         public:
            static const Level DEBUGGING;

            /**
             * Log trace information. This level should be used to
             * log when functions are entered or exited
             */
         public:
            static const Level TRACING;

            /** Log information */
         public:
            static const Level INFO;

            /** This level is used to select logs when an exception is thrown or caught. */
         public:
            static const Level CAUGHT;

            /** This level is used to select logs when an exception is thrown */
         public:
            static const Level THROWING;

            /** Log exceptions information. This level will log both CAUGHT and THROWING. */
         public:
            static const Level EXCEPTION;

            /** Log warning */
         public:
            static const Level WARN;

            /** A severe error log. This one will always be logged. */
         public:
            static const Level SEVERE;

            /** The log level that is off. nothing will be logged */
         public:
            static const Level OFF;

            /** A bug */
         public:
            static const Level BUG;

            /**
             * Create a log level with level 0
             */
         public:
            constexpr inline Level()
                  : _level(0)
            {
            }

            /**
             * Create a log level with the specified value
             * @param lid a level
             */
         public:
            constexpr inline explicit Level(::std::int8_t lid)
                  : _level(lid)
            {
            }

            /**
             * Copy constructor.
             * @param x a log level
             */
         public:
            constexpr inline Level(const Level& x)
                  : _level(x._level)
            {
            }

            /**
             * Copy constructor.
             * @param x a log level
             */
         public:
            constexpr inline Level(Level&& x)
                  : _level(x._level)
            {
            }

         public:
            inline ~Level()
            {
            }

            /**
             * Get the level id. This method should primarily
             * be used by functions that know how to map a log
             * level into a string.
             * @return the log level index
             */
         public:
            constexpr inline ::std::int8_t id() const
            {
               return _level;
            }

            /**
             * Copy operator
             */
         public:
            constexpr inline Level& operator=(const Level& l)
            {
               _level = l._level;
               return *this;
            }

            /**
             * Copy operator
             */
         public:
            constexpr inline Level& operator=(Level&& l)
            {
               _level = l._level;
               return *this;
            }

            /**
             * Compare two log levels
             * @param lx a level
             * @return -1 if this level logs less than lx,
             *          0 if this level logs the same amount as lx,
             *          1 if this level logs more than lx
             */
         public:
            constexpr inline int compare(const Level& lx) const
            {
               return _level < lx._level ? -1 : (_level == lx._level ? 0 : 1);
            }

            /**
             * Test if this level is less than the specified level
             * @param lx a log level
             * @return true if this->compare(lx) < 0
             */
         public:
            constexpr inline bool less(const Level& lx) const
            {
               return _level < lx._level;
            }

            /**
             * Test if this level is equal to the specified level.
             * @param lx a log level
             * @return true if this->compare(lx) == 0
             */
         public:
            constexpr inline bool equals(const Level& lx) const
            {
               return _level == lx._level;
            }

            /**
             * The less-than operator
             * @param lx a level
             * @return this->less(lx)
             */
         public:
            constexpr inline bool operator<(const Level& lx) const
            {
               return less(lx);
            }

            /**
             * The less-than-or-equal operator
             * @param lx a level
             * @return this->less(lx)
             */
         public:
            constexpr inline bool operator<=(const Level& lx) const
            {
               return _level <= lx._level;
            }

            /**
             * The greater-than operator
             * @param lx a level
             * @return this->less(lx)
             */
         public:
            constexpr inline bool operator>(const Level& lx) const
            {
               return _level > lx._level;
            }

            /**
             * The greater-than-or-equal operator
             * @param lx a level
             * @return this->less(lx)
             */
         public:
            constexpr inline bool operator>=(const Level& lx) const
            {
               return _level >= lx._level;
            }

            /**
             * The equality operator
             * @param lx a level
             * @return this->equals(lx)
             */
         public:
            constexpr inline bool operator==(const Level& lx) const
            {
               return _level == lx._level;
            }

            /**
             * The inequality operator
             * @param lx a level
             * @return this->equals(lx)
             */
         public:
            constexpr inline bool operator!=(const Level& lx) const
            {
               return _level != lx._level;
            }

            /**
             * If the level is one of the default levels, then map it into a string.
             * @param x a log level
             * @return a string or nullptr if this is not a default level
             */
         public:
            static const char* getName(Level x);

            /**
             * Get the level by the specified name. If there is no level
             * by that name, then an error is raised.
             * @param name a level
             * @return the level
             * @throws ::std::invalid_argument if name == nullptr
             * @throws ::std::invalid_argument if the name does not correspond to a valid level
             */
         public:
            static Level getLevel(const char* name);

            /**
             * Get the level by the specified name. If there is no level
             * by that name, then an error is raised.
             * @param name a level
             * @return the level
             * @throws ::std::invalid_argument if name == nullptr
             * @throws ::std::invalid_argument if the name does not correspond to a valid level
             */
         public:
            static Level getLevel(const ::std::string& name);

            /** The level */
         private:
            ::std::int8_t _level;
      };
   }
}

#endif
