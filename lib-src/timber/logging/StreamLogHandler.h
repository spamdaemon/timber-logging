#ifndef _TIMBER_LOGGING_STREAMLOGHANDLER_H
#define _TIMBER_LOGGING_STREAMLOGHANDLER_H

#ifndef _TIMBER_LOGGING_CONFIGURABLELOGHANDLER_H
#include <timber/logging/ConfigurableLogHandler.h>
#endif

#include <mutex>
#include <iosfwd>

namespace timber {
   namespace logging {

      /**
       * This log handler writes any log record directly to an output stream. Since that
       * stream may be buffered, this class implements a flush function which will flush the stream.
       * Using setAutoFlushEnabled(), clients can cause every log entry to be flushed after it
       * has been written. This class is thread-safe and will serialize entries logged concurrently.
       * <br>
       * Subclasses of must invoke the setStream() method to set the output stream, otherwise all
       * LogRecords will be discarded.
       */
      class StreamLogHandler : public ConfigurableLogHandler
      {
            /** @name Disable copying
             * @{
             */
         private:
            StreamLogHandler(const StreamLogHandler&) = delete;
            StreamLogHandler& operator=(const StreamLogHandler&) = delete;
            /*@}*/

            /**
             * Create a new log stream handler. Automatic flushing is disabled.
             */
         public:
            StreamLogHandler();

            /**
             * Destructor. Clients should reset the stream to 0,
             * but it is not strictly necessary. This method does
             * absolutely nothing with the stream.
             */
         public:
            ~StreamLogHandler();

            /**
             * Flush the current stream.
             */
         public:
            void flush();

            /**
             * Set this handler to flush the stream each time a log entry was written.
             * @param enabled true to enable automatic flushing, false to disable
             */
         public:
            void setAutoFlushEnabled(bool enabled);

            /**
             * Test if automatic flushing is enabled.
             * @return true if log entry are flushed automatically to the stream
             */
         public:
            inline bool isAutoFlushEnabled() const
            {
               return _autoflush;
            }

            /**
             * Write the log entry to the current stream.
             * @param e a log entry
             */
         public:
            void processLogEntry(const ::std::shared_ptr< LogRecord>& e);

            /**
             * Set a new output stream. This class does not manage the pointer,
             * but merely uses it. Clients should call setStream() in their destructor.
             * @param s a new output stream or 0 to disable output
             */
         protected:
            void setStream(::std::ostream* s);

            /** A mutex is need to synchronize access to the log */
         private:
            ::std::mutex _mutex;

            /** The stream */
         private:
            volatile ::std::ostream* _stream;

            /** True to autoflush the stream every time an entry is made */
         private:
            bool _autoflush;
      };
   }
}

#endif
