#ifndef _TIMBER_LOGGING_TIMESTAMP_H
#define _TIMBER_LOGGING_TIMESTAMP_H

#include <chrono>

namespace timber {
   namespace logging {

      /** The timestamp type */
      typedef ::std::chrono::system_clock::time_point Timestamp;

   }
}

#endif
