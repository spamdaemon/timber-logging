#ifndef _TIMBER_LOGGING_LOGIMPL_H
#define _TIMBER_LOGGING_LOGIMPL_H

#ifndef _TIMBER_LOGGING_LEVEL_H
#include <timber/logging/Level.h>
#endif

#ifndef _TIMBER_LOGGING_LOGLEVEL_H
#include <timber/logging/LogLevel.h>
#endif

#include <vector>
#include <memory>
#include <stdexcept>
#include <mutex>

namespace timber {
   namespace logging {

      /** A log entry */
      class LogRecord;

      /** A log handler */
      class LogHandler;

      /** The log factory */
      class LogFactory;

      /**
       * The implementation of a log
       */
      class LogImpl : public ::std::enable_shared_from_this< LogImpl>
      {
         private:
            LogImpl() = delete;
            LogImpl(const LogImpl&) = delete;
            LogImpl& operator=(const LogImpl&) = delete;

            /** The handlers */
         public:
            typedef ::std::vector< ::std::shared_ptr< LogHandler> > Handlers;

            /** A reference to a log impl */
         public:
            typedef ::std::shared_ptr< LogImpl> Ref;

            /**
             * Create a new named log with an initial log level.
             * @param nm the name of the logger
             * @param initLevel the initial level
             */
         public:
            LogImpl(::std::string nm, const Level& initLevel);

            /** Destructor */
         public:
            virtual ~LogImpl();

            /**
             * Create a log based on this log object.
             * @param nm the name of the new log
             */
         public:
            virtual ::std::shared_ptr< LogImpl> createChildLog(::std::string nm);

            /**
             * Get the name of the log. Clients should copy the value out
             * as soon as possible.
             * @return the name of the log.
             */
         public:
            inline const ::std::string& name() const
            {
               return const_cast< const ::std::string&>(_name);
            }

            /**
             * Add a log handler.
             * @param handler a log handler
             * @throws ::std::exception on error
             */
         public:
            void addLogHandler(::std::shared_ptr< LogHandler> handler);

            /**
             * Remove the specifeAdd a log handler.
             * @param handler a log handler.
             */
         public:
            void removeLogHandler(::std::shared_ptr< LogHandler> handler);

            /**
             * Remove all handlers.
             */
         public:
            void removeLogHandlers();

            /**
             * Set the log level.
             * @param newLevel the new log level
             */
         public:
            void setLevel(const Level& newLevel);

            /**
             * Get the log level
             * @return the current log level
             */
         public:
            ::std::shared_ptr< const LogLevel> level() const;

            /**
             * Determine if entries at the specified log level will be committed to this log.
             * Only levels at or below the current log's log level will be considered.
             * This method must not return true if the level exceeds the current log level.
             * @param loglevel a log level
             * @return true if the entries will be logged at the specified level
             */
         public:
            inline bool isLoggable(Level loglevel) const
            {
               return _level->isLoggable(loglevel);
            }

            /**
             * Log the specified entry. A mutex will have been locked to ensure that this method
             * is not accessed concurrently. If there are an LogHandlers, then the entry
             * is passed to each in turn for processing. The log handler must not invoke
             * this log while it is processing otherwise a deadlock may occur.
             * @param entry a log entry
             * @return a log entry or 0 if the level is not loggable
             */
         public:
            void log(::std::shared_ptr< LogRecord> entry);

            /**
             * Log the specified entry. A mutex will have been locked to ensure that this method
             * is not accessed concurrently. If there are an LogHandlers, then the entry
             * is passed to each in turn for processing. The log handler must not invoke
             * this log while it is processing otherwise a deadlock may occur.
             * @param entry a log entry a log level
             * @return a log entry or 0 if the level is not loggable
             */
         protected:
            virtual void logEntry(::std::shared_ptr< LogRecord> entry);

            /**
             * Set the log factory method.
             * @param m a log factory method
             */
         public:
            static void setFactory(::std::shared_ptr< LogFactory> m);

            /**
             * Create a new log.
             * @param nm the name of the new log.
             * @param level the level
             * @return a new log or nullptr
             */
         public:
            static ::std::shared_ptr< LogImpl> create(::std::string nm, Level level);

            /** The logger's name */
         private:
            const ::std::string _name;

            /** The log level */
         private:
            ::std::shared_ptr< LogLevel> _level;

            /** A mutex */
         private:
            ::std::mutex _mutex;

            /** A vector of log handlers */
         private:
            ::std::unique_ptr< Handlers> _handlers;
      };

   }
}

#endif
