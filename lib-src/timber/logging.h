#ifndef _TIMBER_LOGGING_H

#ifndef _TIMBER_LOGGING_TIMESTAMP_H
#include <timber/logging/Timestamp.h>
#endif

#ifndef _TIMBER_LOGGING_LOG_H
#include <timber/logging/Log.h>
#endif

#ifndef _TIMBER_LOGGING_LOGCONTEXT_H
#include <timber/logging/LogContext.h>
#endif

#ifndef _TIMBER_LOGGING_LOGENTRY_H
#include <timber/logging/LogEntry.h>
#endif

#ifndef _TIMBER_LOGGING_LEVEL_H
#include <timber/logging/Level.h>
#endif

#ifndef _TIMBER_LOGGING_FILELOCATION_H
#include <timber/logging/FileLocation.h>
#endif

#endif
