#include <timber/logging.h>
#include <iostream>

using namespace timber;
using namespace timber::logging;

void test1()
{
   LogEntry(Level::INFO).stream() << "Hello, World" << commit;
   LogEntry(Level::WARN).stream() << "Hello, World " << INT64_C(0x7fffffffffff) << commit;
   LogEntry e(Level::ALL);
   e << "Hello, World" << commit;
   e << "2Hello, World" << commit;
}

void test2()
{
   Log log("test2");
   log.warn("Hello, World");
   Log().warn("HOLA");
}

void test3()
{
   Log log;
   try {
      ::std::runtime_error rt("error");
      log.throwing("Throwing exception", rt);
      throw rt;
   }
   catch (const ::std::exception& e) {
      log.caught("exception thrown", e);
   }
}



void test4()
{
   const char* MESSAGE = "Hello";

   Log myLog("MyLog");
   myLog.setLevel(Level::TRACING);
   myLog.log(Level::INFO, [] (::std::ostream& out) {out << "Hello";});
   myLog.info([] (::std::ostream& out) {out << "Hello";});
   myLog.warn([] (::std::ostream& out) {out << "Hello";});
   myLog.severe([] (::std::ostream& out) {out << "Hello";});
   myLog.tracing([ &MESSAGE] (::std::ostream& out) {out << MESSAGE;});
   myLog.bug([] (::std::ostream& out) {out << "Hello";});
   myLog.bug([] () { return  "Hello, bug";});
}

void test5()
{
   Log myLog(nullptr);
   myLog.severe("X Message");
}

void test6()
{
   Log myLog{::std::shared_ptr<LogImpl>()};
   myLog.severe("FAILED Message");
}

void test7()
{
   Log foo("foo");
   Log bar=foo.create("bar");
   Log baz=bar.create("baz");

   foo.setLevel(Level::SEVERE);
   bar.setLevel(Level::WARN);
   baz.setLevel(Level::INFO);

   baz.info("info-log");
   baz.warn("warn-log");
   baz.severe("severe-log");
}


int main()
{
   Log().setLevel(Level::ALL);
   test1();
   test2();
   test3();
   test4();
   test5();
   test6();
   test7();
   return 0;
}
