#include <timber/logging/Log.h>
#include <timber/logging/LogEntry.h>


using namespace timber;
using namespace timber::logging;

void test1()
{
  LogEntry e;
  e.reset(Level::INFO) << "Hello, World" << commit;
  e.reset(Level::WARN) << "Hello, World " << INT64_C(0x7fffffffffff) << commit;
  e.reset(Level::ALL);
  e << "Hello, World" <<  doLog;
  e << "2Hello, World" << doLog;
}

void test2()
{
  LogEntry().info() << "Info" << commit;
  LogEntry().warn() << "Warn" << commit;
  LogEntry().tracing() << "Tracing" << commit;

  
  Log log("test2");
  LogEntry().warn(log) << "Hello, World" << commit;
  LogEntry().warn(log) << "HOLA" << commit;
}

void test3()
{
	Log log("test3");
	LogEntry().info () << "Type: " << typeid(LogEntry) << commit;
	LogEntry().warn (log) << "Type: " << typeid(LogEntry) << commit;
}

int main()
{
  Log::rootLog().setLevel(Level::INFO);
  test1();
  test2();
  test3();
  return 0;
}
