#include <timber/logging/macros.h>
#include <cassert>
#include <iostream>

using namespace timber;
using namespace timber::logging;

void test1()
{
   START_LOG("foo.bar",WARN) << "Hello, World!" << END_LOG;
}

void test2()
{
   const char* msg="My new log message";
   LOG_MESSAGE("foo.bar",INFO,msg);
}

int main()
{
   test1();
   test2();

   return 0;
}
