#include <timber/logging.h>
#include <iostream>
#include <thread>

using namespace timber;
using namespace timber::logging;

void test1()
{
   LogContext::log().info("Hello");
}

void test2()
{
   Log outer("*OUTER*");
   LogContext g(outer);
   {
      LogContext::log().log(Level::INFO, "MESSAGE 1 outer");
      Log inner("*INNER*");
      const LogContext h(inner);
      {
         LogContext::log().log(Level::INFO, "MESSAGE 2 inner");
      }
      LogContext::log().log(Level::INFO, "MESSAGE 3 inner");
   }
   LogContext::log(Level::INFO, "MESSAGE 4 outer");
}

void test3()
{
   auto proc = [] {LogEntry(LogContext::log()).info() << "Hello " << ::std::this_thread::get_id() << endLog;};
   ::std::thread t1(proc);
   ::std::thread t2(proc);
   ::std::thread t3(proc);

   t1.join();
   t2.join();
   t3.join();
}

int main()
{
   test1();
   test2();
   test3();

   return 0;
}
