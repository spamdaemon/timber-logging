#include <timber/logging/Registry.h>
#include <timber/logging/Log.h>
#include <timber/logging/LogEntry.h>
#include <string>
#include <iostream>
#include <sstream>
#include <cassert>

using namespace timber;
using namespace timber::logging;

static const size_t LOG_COUNT = 3;
static int logged[LOG_COUNT];

::std::string parseString(int i)
{
   ::std::ostringstream ss;
   ss << i;
   return ss.str();
}

struct xLog : public LogImpl
{
      xLog(int i)
            : LogImpl(parseString((int) i).c_str(), Level::MAX), _index(i)
      {
         logged[i] = 0;
      }
      ~xLog()
      {
      }
      void logEntry(::std::shared_ptr< LogRecord>)
      {
         ++logged[_index];
      }
   private:
      const int _index;
};

typedef ::std::vector< ::std::shared_ptr< LogImpl> > Loggers;

void test1()
{
   for (size_t i = 0; i < LOG_COUNT; ++i) {
      auto tmp = ::std::make_shared<xLog>(i);
      Registry::registerLog(tmp);
   }

   for (size_t i = 0; i < LOG_COUNT; ++i) {
      assert(!logged[i]);
   }

   for (size_t i = 0; i < LOG_COUNT; ++i) {
      Log tmp(parseString((int) i).c_str());
      tmp.log(Level(i), "Some log entry");
   }

   for (size_t i = 0; i < LOG_COUNT; ++i) {
      assert(logged[i]);
   }

   Loggers logs = Registry::logs();
   for (size_t i = 0; i < LOG_COUNT; ++i) {
      bool found = false;
      for (Loggers::const_iterator j = logs.begin(); j != logs.end(); ++j) {
         if ((*j)->name() == parseString((int) i).c_str()) {
            found = true;
            break;
         }
      }
      assert(found);
   }

   for (Loggers::const_iterator i = logs.begin(); i != logs.end(); ++i) {
      ::std::cerr << "Log " << (*i)->name() << ::std::endl;
   }

}

int main()
{
   test1();
   return 0;
}
