#include <timber/logging/LogLevel.h>
#include <cassert>
#include <iostream>

using namespace timber;
using namespace timber::logging;

void test_OFF()
{
   LogLevel OFF(Level::OFF);

   assert(!OFF.isLoggable(Level::OFF));
   assert(!OFF.isLoggable(Level::ALL));
}

void test_INFO()
{
   LogLevel INFO(Level::INFO);

   assert(INFO.isLoggable(Level::INFO));
   assert(!INFO.isLoggable(Level(Level::INFO.id() + 1)));
}

int main()
{
   test_OFF();
   test_INFO();

   return 0;
}
